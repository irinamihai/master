Distributed text editor implemented using CBCAST, ABCAST and 3Phase
------------------------------------------------------------------
For choosing the number of sites, vary the numbers in bin/runit.sh, from:
sequence=$(seq 50000 50002) (This is an example for three sites)

For choosing the algorithm, in bin/runit.sh, modify:
    - CB for CBCAST
    - AB for ABCAST
    - TO for 3Phase
    
For running the application just issue "./runit.sh" in a terminal window.
