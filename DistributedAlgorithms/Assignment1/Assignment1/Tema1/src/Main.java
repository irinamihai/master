import javax.swing.SwingUtilities;

public class Main {
	/**
	 * @param args
	 * @throws Exception
	 */

	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			System.out.println("Usage: [..] <CB|AB|3> <Port>");
			return;
		}

		int myport = Integer.parseInt(args[1]);

		UIModule ui = new UIModule(myport);
		SwingUtilities.invokeAndWait(ui);
		
		int[] ports = new int[args.length - 2];
		Server server;
		try {
			server = new Server(myport, ports.length);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		Client[] clients = new Client[ports.length];
		for (int i = 0; i < ports.length; i++) {
			ports[i] = Integer.parseInt(args[i + 2]);
			clients[i] = new Client(myport, ui);
			clients[i].connect(ports[i]);
		}

		if (args[0].equals("CB")) {
			server.algorithm = new CBCAST(myport, clients, ui);
		}
		else if (args[0].equals("AB")) {
			server.algorithm = new ABCAST(myport, clients, ui);
		}
		else if (args[0].equals("TO")) {
			server.algorithm = new TotalOrder(myport, clients, ui);
		}

		server.start();

		Driver simulatorDriver = new Driver(server.algorithm, ui);
		simulatorDriver.readCommandsFrom("commands10000.txt");
	}
}
