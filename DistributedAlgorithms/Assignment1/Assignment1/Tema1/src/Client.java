import java.net.*;
import java.util.LinkedList;
import java.util.List;
import java.io.*;

public class Client {
	private Socket client;
	private OutputStream outToServer;
	private final int myport;
	private Boolean connected = false;
	List<String> sent = new LinkedList<String>(); // for debugging

	public Client(int myport, UIModule ui) {
		this.myport = myport;
	}

	public void connect(int port) {
		if (connected)
			return;

		for (int i = 0; i < 20; i++) {
			try {
				client = new Socket("127.0.0.1", port);
				outToServer = client.getOutputStream();
				connected = true;
				break;
			} catch (IOException e) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
				}
			}
		}
	}

	public int getPort() {
		return client.getPort();
	}

	@SuppressWarnings("unused")
	private void disconnect() {
		try {
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendMessage(String message) {
		DataOutputStream out = new DataOutputStream(outToServer);
		try {
			out.writeUTF(message);
			// if (c.Type == Command._Type.EOF) {
			// disconnect();
			// }
			sent.add(message);
		} catch (IOException e) {
			for (int i = 0; i < sent.size(); i++) {
				System.out.println(sent.get(i));
			}
			System.out.println("From " + myport + " to " + client.getPort()
					+ ", " + message);
			e.printStackTrace();
		}
	}
}