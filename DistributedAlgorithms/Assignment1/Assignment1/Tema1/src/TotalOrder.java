import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;
import java.util.Collections;

public class TotalOrder extends Algorithm {
	int clock;
	int priority;
	HashMap<String, Integer> receivedProposedTSNumber;
	HashMap<String, Integer> receivedProposedTSTimestamp;
	HashMap<String, Command> sentCommands;
	LinkedList<TotalOrderMessage> sentMessages;
	LinkedList<TotalOrderMessage> temp_Q;
	LinkedList<TotalOrderMessage> delivery_Q;

	public TotalOrder(int myport, Client[] clients, UIModule ui) {
		super(myport, clients, ui);
		clock = 0;
		priority = 0;
		temp_Q = new LinkedList<>();
		delivery_Q = new LinkedList<>();
		receivedProposedTSNumber = new HashMap<String, Integer>();
		receivedProposedTSTimestamp = new HashMap<String, Integer>();
		sentCommands = new HashMap<String, Command>();
		sentMessages = new LinkedList<>();
	}

	synchronized public void receiveMessage(String receivedString, String uid) {
		TotalOrderMessage receivedMessage = new TotalOrderMessage();
		try {
			receivedMessage.Deserialize(receivedString);

			switch (receivedMessage.messageType) {
			case REVISE_TS:
				handleReviseTSMessage(receivedMessage);
				break;
			case PROPOSED_TS:
				handleProposedTSMessage(receivedMessage);
				break;
			case FINAL_TS:
				handleFinalTSMessage(receivedMessage);
				break;
			default:
				throw new Exception("Unknown message type received!");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	synchronized public void handleReviseTSMessage(
			TotalOrderMessage receivedMessage) {

		ui.debug("[Revise<<" + receivedMessage.senderId + "] "
				+ receivedMessage.Serialize());

		// get (new) priority
		priority = Math.max(priority + 1, clock);

		// change message timestamp with the new priority
		receivedMessage.timestamp = priority;

		// add modified message to the temp_Q, at the end
		if (receivedMessage.command.type != Command.Type.EMPTY)
			temp_Q.addLast(receivedMessage);

		// send back the new message
		sendProposedTS(receivedMessage);
	}

	synchronized public void handleProposedTSMessage(
			TotalOrderMessage receivedMessage) {
		/*
		 * go through receivedProposedTS an find out for which message the
		 * ProposedTS is
		 */
		ui.debug("[Prop<<]" + receivedMessage.Serialize());

		String currentUid = receivedMessage.uid;

		/*
		 * if the proposedTS message received has a greater value than the
		 * current timestamp, update timestamp
		 */

		receivedProposedTSTimestamp.put(currentUid, Math.max(
				receivedProposedTSTimestamp.get(currentUid),
				receivedMessage.timestamp));

		/* update the number of received proposedTS messages for the current uid */
		receivedProposedTSNumber.put(currentUid,
				receivedProposedTSNumber.get(currentUid) + 1);

		/* check if all the expected proposedTS messages have been received */
		if (receivedProposedTSNumber.get(currentUid) == clients.length) {
			ui.debug("[Final>>]" + receivedMessage.Serialize());
			sendFinalTS(currentUid);
			clock = Math
					.max(clock, receivedProposedTSTimestamp.get(currentUid));
		}
	}

	synchronized public void handleFinalTSMessage(
			TotalOrderMessage receivedMessage) {

		ui.debug("[Final<<]" + " " + receivedMessage.Serialize());

		// identify the received message in temp_Q
		Integer currentIndex = -1;

		/*
		 * Print temp_Q for debugging ui.debug("temp_Q: "); for (int i = 0; i <
		 * temp_Q.size(); i++) ui.debug(temp_Q.get(i).command.type + " " +
		 * temp_Q.get(i).uid + " " + temp_Q.get(i).timestamp.toString());
		 * 
		 * ui.debug("Se cauta: " + receivedMessage.uid);
		 */

		for (int i = 0; i < temp_Q.size(); i++) {
			if (temp_Q.get(i).uid.equals(receivedMessage.uid)) {
				currentIndex = i;
				break;
			}
		}

		/* If no message of the given ui found, nothing to do */
		if (currentIndex == -1)
			return;

		temp_Q.get(currentIndex).deliverable = true;
		temp_Q.get(currentIndex).timestamp = receivedMessage.timestamp;

		/* resort temp_Q based on timestamps */
		Collections.sort(temp_Q);

		// if (temp_Q.getFirst().uid.equals(receivedMessage.uid)) {
		// delivery_Q.add(temp_Q.getFirst());
		// temp_Q.removeFirst();

		/*
		 * while the head of temp_Q is deliverable, add the head to delivery_Q
		 */
		while (temp_Q.size() > 0 && temp_Q.getFirst().deliverable == true) {
			delivery_Q.add(temp_Q.getFirst());
			temp_Q.removeFirst();
		}
		// }

		/* call deliver function */
		if (delivery_Q.size() > 0) {
			/*
			 * Print deliver queue before delivery
			 * ui.debug("D_Q: ");
			 * for (int i = 0; i < delivery_Q.size(); i++) {
			 * ui.debug(delivery_Q.get(i).Serialize()); }
			 */
			deliver();
		}

		/*
		 * Print size of queues for debugging ui.debug("TSIZE: " +
		 * temp_Q.size()); for (int i = 0; i < temp_Q.size(); i++)
		 * ui.debug(temp_Q.get(i).deliverable.toString()); ui.debug("QSIZE: " +
		 * delivery_Q.size());
		 */
	}

	synchronized public void deliver() {
		Collections.sort(sentMessages);
		while (delivery_Q.size() > 0) {
			ui.executeCommand(delivery_Q.getFirst().command);
			clock = Math.max(clock, delivery_Q.getFirst().timestamp) + 1;
			delivery_Q.removeFirst();
		}
	}

	synchronized public void sendProposedTS(TotalOrderMessage newMessage) {

		final Integer client = newMessage.senderId - 50000;
		newMessage.senderId = myport;
		newMessage.messageType = TotalOrderMessage.MessageType.PROPOSED_TS;
		final String str = newMessage.Serialize();
		final Random rand = new Random();

		ui.debug("[Prop>>" + newMessage.senderId + "] " + str);

		final Thread thr = new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(rand.nextInt(401) + 100);
					clients[client].sendMessage(str);
				} catch (Exception e) {
				}
			}
		});
		thr.start();
	}

	synchronized public void sendFinalTS(String uid) {
		TotalOrderMessage finalMessage = new TotalOrderMessage();
		finalMessage.senderId = myport;
		finalMessage.uid = uid;
		finalMessage.timestamp = receivedProposedTSTimestamp.get(uid);
		finalMessage.messageType = TotalOrderMessage.MessageType.FINAL_TS;
		finalMessage.command = sentCommands.get(uid);
		sentMessages.add(finalMessage);

		broadcast(finalMessage, true);
	}

	synchronized public Message sendCommand(Command c, Boolean bcast) {

		if (c.type == Command.Type.EMPTY) {
			return null;
		}

		TotalOrderMessage totalOrderMessage = new TotalOrderMessage();

		clock++;
		/* create the message to be sent */
		totalOrderMessage.senderId = this.myport;
		totalOrderMessage.timestamp = this.clock;
		totalOrderMessage.deliverable = false;
		totalOrderMessage.uid = "" + this.myport + "-" + clock;
		totalOrderMessage.command = c;
		totalOrderMessage.messageType = TotalOrderMessage.MessageType.REVISE_TS;

		/*
		 * add the new element in receivedProposedTSNumber and
		 * receivedProposedTSTimestamps and initialize them with 0 to keep a
		 * track of the ProposedTS messages received
		 */
		receivedProposedTSNumber.put(totalOrderMessage.uid, 0);
		receivedProposedTSTimestamp.put(totalOrderMessage.uid, 0);

		/* add sent command to sendCommands HashMap */
		System.out.println("Comanda: " + totalOrderMessage.command.Serialize());
		sentCommands.put(totalOrderMessage.uid, totalOrderMessage.command);

		if (bcast)
			broadcast(totalOrderMessage, false);

		return null;
	}

	synchronized public void broadcast(TotalOrderMessage msg,
			Boolean finalMessage) {

		final String str = msg.Serialize();
		final Random rand = new Random();

		ui.debug("[Revise>>] " + str);

		for (int i = 0; i < clients.length; i++) {
			if (clients[i].getPort() == myport) {
				// if (finalMessage)
				clients[i].sendMessage(str);
			} else {
				final int clientIndex = i;
				final Thread thr = new Thread(new Runnable() {
					public void run() {
						try {
							Thread.sleep(rand.nextInt(401) + 100);
							clients[clientIndex].sendMessage(str);
						} catch (Exception e) {
						}
					}
				});
				thr.start();
			}
		}
	}
}
