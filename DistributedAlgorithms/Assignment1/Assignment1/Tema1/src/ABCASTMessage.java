public class ABCASTMessage {
	String uid;
	String[] order;

	synchronized public String Serialize() {
		if (order == null)
			return "null@" + uid;

		String str = new String();
		for (int i = 0; i < order.length; i++) {
			str += order[i] + "@";
		}

		str += uid;

		return str;
	}

	synchronized public void Deserialize(String str) throws Exception {
		String[] abtokens = str.split("@");
		if (abtokens[0].equals("null")) {
			order = null;
		} else {
			order = new String[abtokens.length - 1];

			for (int i = 0; i < abtokens.length - 1; i++)
				order[i] = abtokens[i];
		}
		uid = abtokens[abtokens.length - 1];
	}

}
