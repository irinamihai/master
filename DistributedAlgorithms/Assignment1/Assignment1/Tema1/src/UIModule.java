import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

class UIModule implements Runnable {
	private JTextArea JTA;
	private JTextArea JTADebug;
	private Boolean started = false;
	private final int myport;

	public UIModule(int myport) {
		this.myport = myport;
	}

	@Override
	public void run() {
		// Create the window
		JFrame f = new JFrame("UI - " + myport);
		// Sets the behavior for when the window is closed
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Add a layout manager so that the button is not placed on top of the
		// label
		f.setLayout(new GridLayout(0, 1));
		// Add a label and a button
		Font font = new Font("Arial", Font.BOLD, 14);
		JTA = new JTextArea("", 20, 30);
		JTA.setFont(font);
		JTA.setForeground(Color.BLACK);
		JTA.setEnabled(false);
		f.add(JTA);

		JTADebug = new JTextArea("", 20, 30);
		JTADebug.setFont(font);
		JTADebug.setForeground(Color.BLACK);
		JTADebug.setEnabled(false);
		JScrollPane sp = new JScrollPane(JTADebug);
		f.add(sp);
		// Arrange the components inside the window
		f.pack();
		// By default, the window is not visible. Make it visible.
		f.setVisible(true);
		started = true;
	}

	synchronized public void executeCommand(Command c) {
		while (!started)
			;

		if (c.type == Command.Type.Ins) {
			String currentText = JTA.getText();
			int pos = Math.min(c.position, currentText.length());
			pos = Math.max(0, pos);
			JTA.setText(currentText.substring(0, pos) + c.character
					+ currentText.substring(pos));
		} else if (c.type == Command.Type.Del) {
			String currentText = JTA.getText();
			int pos = Math.min(c.position, currentText.length());
			pos = Math.max(0, pos);
			JTA.setText(currentText.substring(0, pos) + currentText.substring(pos + 1));
		}
	}

	synchronized public void debug(String str) {
		while (!started)
			;

		JTADebug.setText(JTADebug.getText() + str + "\n");
		JTADebug.setCaretPosition(JTADebug.getDocument().getLength());
	}
}
