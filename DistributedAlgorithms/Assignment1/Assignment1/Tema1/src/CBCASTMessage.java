public class CBCASTMessage {
	int from;
	int[] timestamps;
	Boolean deliverable;

	public CBCASTMessage() {
		deliverable = true;
	}

	synchronized public String Serialize() {
		String str = new String();
		str += from;
		for (int i = 0; i < timestamps.length; i++)
			str += "@" + timestamps[i];

		str += "@" + deliverable;
		return str;
	}

	synchronized public void Deserialize(String str) throws Exception {
		String[] cbtokens = str.split("@");

		from = Integer.parseInt(cbtokens[0]);

		timestamps = new int[cbtokens.length - 2];
		for (int i = 0; i < timestamps.length; i++)
			timestamps[i] = Integer.parseInt(cbtokens[1 + i]);

		deliverable = Boolean.parseBoolean(cbtokens[cbtokens.length - 1]);
	}
}
