#!/bin/bash
killall java 2>&1 > /dev/null
cd ../bin
sequence=$(seq 50000 50002)
for i in $sequence
do
    myport=$i
    java -cp ../bin Main Jupiter $myport $sequence &
done
