import java.util.LinkedList;

public class ABCAST extends CBCAST {
	private Boolean hasToken = false;
	private LinkedList<String> setsOrderArray = new LinkedList<String>();
	private int tokenTime = 1;

	public ABCAST(int myport, Client[] clients, UIModule ui) {
		super(myport, clients, ui);
		if (myport == keyset[0])
			hasToken = true;
	}

	@Override
	synchronized public Message sendCommand(Command c, Boolean bcast) {
		Message m = super.sendCommand(c, false);

		m.abmessage = new ABCASTMessage();
		/* define uid as: local port-my timestamp */
		m.abmessage.uid = myport + "-" + super.VT.get(myport);

		if (hasToken) {
			if (setsOrderArray.isEmpty()) {
				m.abmessage.order = new String[1];
				m.abmessage.order[0] = m.abmessage.uid;
			} else {
				if (setsOrderArray.get(0).split("-")[0].equals("" + myport))
					setsOrderArray.remove(0);
				setsOrderArray.add(m.abmessage.uid);
				m.abmessage.order = new String[setsOrderArray.size()];
				setsOrderArray.toArray(m.abmessage.order);
				setsOrderArray.clear();
			}
		} else
			m.abmessage.order = null;

		if (bcast)
			broadcast(m);

		if (c.type == Command.Type.EOF)
			gotEOF = true;

		return m;
	}

	Boolean gotEOF = false;

	@Override
	synchronized void deliver(Message msg, String uid, String from) {
		super.deliver(msg, uid, from);
		if (!uid.isEmpty())
			setsOrderArray.add(uid);

		if (msg.cbmessage.from != myport && gotEOF && hasToken) {
			Command c = new Command();
			c.type = Command.Type.EMPTY;
			sendCommand(c, true);
		}
	}

	@Override
	synchronized public void receiveMessage(String msg, String uid) {
		try {
			Message m = new Message();
			m.Deserialize(msg);

			if (hasToken)
				m.cbmessage.deliverable = true;
			else
				m.cbmessage.deliverable = false;

			super.receiveMessage(m.Serialize(), m.abmessage.uid);

			if (!hasToken) {
				Boolean stopping = false;
				while (!stopping) {
					stopping = true;
					int tokenHolder = -1;
					Message abmessage = null;
					for (int i = 0; i < delayQueue.size(); i++) {
						/* looking for a message that came from the token holder */
						if (delayQueue.get(i).abmessage.order != null) {
							abmessage = delayQueue.get(i);
							/* find out if the message from the token holder is the expected one */
							if (tokenTime == abmessage.cbmessage.timestamps[abmessage.cbmessage.from - 50000]) {
								tokenHolder = abmessage.cbmessage.from - 50000;
								break;
							}	
						}
					}
					if(tokenHolder == -1)
						break; // we don't have a pending ABMessage ordering in queue
					
					// check if we have received all previous messages in abmessage.order
					Boolean allInQueue = true;
					for (int i = 0; i < abmessage.abmessage.order.length; i++) {
						Boolean hasThisOne = false;
						for (int j = 0; j < delayQueue.size(); j++) {
							if(abmessage.abmessage.order[i].equals(delayQueue.get(j).abmessage.uid))
							{
								hasThisOne = true;
								break;
							}
						}
						if(!hasThisOne)
						{
							allInQueue = false;
							break;
						}
					}
					
					if(allInQueue)
					{	
						// start delivering
						int orderLength = abmessage.abmessage.order.length;
						for (int i = 0; i < orderLength; i++) {
							Message delivered = null;
							for (int j = 0; j < delayQueue.size(); j++) {
								if(abmessage.abmessage.order[i].equals(delayQueue.get(j).abmessage.uid))
								{
									delivered = delayQueue.remove(j);
									deliver(delivered, "", "**");
									break;
								}
							}
						}
						
						// check for another abmessage
						stopping = false;
						tokenTime++;
					}
					String str = new String();
					for(int i=0; i< delayQueue.size(); i++)
						str += delayQueue.get(i).abmessage.uid + " ";
					ui.debug("Queue=" + str);
					ui.debug("TokenTime=" + tokenTime);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
