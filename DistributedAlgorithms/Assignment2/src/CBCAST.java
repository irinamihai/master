import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

public class CBCAST extends Algorithm {
	HashMap<Integer, Integer> VT;
	protected final LinkedList<Message> delayQueue = new LinkedList<Message>();
	final Integer[] keyset;

	public CBCAST(int myport, Client[] clients, UIModule ui) {
		super(myport, clients, ui);

		VT = new HashMap<Integer, Integer>();
		VT.put(myport, 0);
		for (int i = 0; i < clients.length; i++) {
			VT.put(clients[i].getPort(), 0);
		}

		this.keyset = new Integer[clients.length];
		VT.keySet().toArray(keyset);
		Arrays.sort(keyset);
	}

	synchronized void deliver(Message m, String uid, String from) {
		if(m.c.type == Command.Type.EMPTY)
			return;
		if (m.c.type == Command.Type.EOF) {
			ui.debug("[deliver" + from + "] " + m.Serialize());
		} else {
			ui.debug("[deliver" + from + "] " + m.Serialize());
			ui.executeCommand(m.c);
		}
		for (int i = 0; i < clients.length; i++) {
			VT.put(keyset[i], Math.max(VT.get(keyset[i]), m.cbmessage.timestamps[i]));
		}
	}

	synchronized void delay(Message m) {
		ui.debug("[delay] " + m.Serialize());
		delayQueue.add(m);
	}

	synchronized Boolean needsDelay(Message m) {
		/* for ABCAST part - if message is undeliverable, delay it */
		if (m.cbmessage.deliverable == false)
			return true;
		
		for (int k = 0; k < clients.length; k++) {
			if (keyset[k] == m.cbmessage.from) {
				if (VT.get(m.cbmessage.from) != m.cbmessage.timestamps[k] - 1) {
					return true;
				}
			} else if (VT.get(keyset[k]) < m.cbmessage.timestamps[k]) {
				return true;
			}
		}
		return false;
	}

	@Override
	synchronized public void receiveMessage(String m, String uid) {
		Message message = new Message();
		try {
			message.Deserialize(m);

			if (message.cbmessage.from == myport) {
				if (message.cbmessage.deliverable == true)
					deliver(message, uid, "--");
				else {
					delay(message);
				}
			} else {
				if (needsDelay(message)) {
					delay(message);
				} else {
					deliver(message, uid, "<-");
					for (int i = 0; i < delayQueue.size(); i++) {
						if (!needsDelay(delayQueue.get(i))) {
							Message m1 = delayQueue.remove(i);
							if(m1.abmessage == null)
								deliver(m1, "", "++");
							else
								deliver(m1, m1.abmessage.uid, "++");
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	synchronized public Message sendCommand(Command c, Boolean bcast) {
		Message cm = new Message();
		VT.put(myport, VT.get(myport) + 1);

		cm.c = c;
		cm.cbmessage = new CBCASTMessage();
		cm.cbmessage.timestamps = new int[keyset.length];
		cm.cbmessage.from = myport;

		for (int i = 0; i < keyset.length; i++) {
			cm.cbmessage.timestamps[i] = VT.get(keyset[i]);
		}

		if (bcast)
			broadcast(cm);
		
		return cm;
	}
}
