public class Command {
	enum Type {
		Ins, Del, EOF, EMPTY
	};

	Type type;
	char character;
	int position;

	public Boolean decodeFromFile(String str) throws Exception {
		if (str.isEmpty()) {
			return false;
		}
		String[] tokens = str.split("\\(|,|\\)");
		if (tokens.length == 3 && tokens[0].equalsIgnoreCase("Ins")) {
			type = Type.Ins;
			if (tokens[2].matches("0|[1-9]+[0-9]*")) {
				position = Integer.parseInt(tokens[2]);
			} else
				throw new Exception("Wrong command");

			if (tokens[1].length() == 3) {
				character = tokens[1].toCharArray()[1];
			} else
				throw new Exception("Wrong command");
		} else if (tokens.length == 2 && tokens[0].equalsIgnoreCase("Del")) {
			type = Type.Del;
			if (tokens[1].matches("0|[1-9]+[0-9]*")) {
				position = Integer.parseInt(tokens[1]);
			} else
				throw new Exception("Wrong command");
		} else
			throw new Exception("Wrong command");
		return true;
	}

	public String Serialize() {
		String str = new String();
		str = type.toString();
		if (type == Type.Ins)
			str += " " + position + " " + character;
		else if (type == Type.Del)
			str += " " + position;
		return str;
	}

	public void Deserialize(String str) throws Exception {
		String[] tokens = str.split(" ");
		if (tokens[0].equalsIgnoreCase("Ins")) {
			type = Type.Ins;
			if (tokens[1].matches("0|[1-9]+[0-9]*")) {
				position = Integer.parseInt(tokens[1]);
			} else
				throw new Exception("Wrong command");

			if (tokens[2].length() == 1) {
				character = tokens[2].toCharArray()[0];
			} else
				throw new Exception("Wrong command");
		} else if (tokens[0].equalsIgnoreCase("Del")) {
			type = Type.Del;
			if (tokens[1].matches("0|[1-9]+[0-9]*")) {
				position = Integer.parseInt(tokens[1]);
			} else
				throw new Exception("Wrong command");
		} else if (tokens[0].equalsIgnoreCase("EOF")) {
			type = Type.EOF;
		} else if (tokens[0].equalsIgnoreCase("EMPTY")) {
			type = Type.EMPTY;
		}else
			throw new Exception("Wrong command");
	}
}
