import java.util.ArrayList;

public class DOPTMessage {
	//unique site's id is the port - 50000
	Integer ID;
	//state maps the other site with the number of operations executed from there
	//HashMap<Integer, Integer> state;
	ArrayList<Integer> state;
	//operation
	Command command;
	Command transformedCommand;
	//priority
	public ArrayList<Integer> priorityArray;
	
	public DOPTMessage() {
		//state = new HashMap<Integer, Integer>();
		state = new ArrayList<Integer>();
		command = new Command();
		priorityArray = new ArrayList<Integer>();
	}
	
	synchronized public void Deserialize(String currentString) throws Exception {
		String[] tokens = currentString.split("\\|");
		
		//get sender id
		this.ID = Integer.parseInt(tokens[0]);
		
		//get sender's state
		String[] state_tokens = tokens[1].split(" ");
		for(int i=0; i<state_tokens.length; i++)
			//this.state.put(i, Integer.parseInt(state_tokens[i]));
			this.state.add(i, Integer.parseInt(state_tokens[i]));
			
		//get operation
		this.command.Deserialize(tokens[2]);
		
		//get priority
		String[] prio_tokens = tokens[3].split(" ");
		
		for(int i=0; i<prio_tokens.length; i++)
			this.priorityArray.add(Integer.parseInt(prio_tokens[i]));
	}
	
	synchronized public String Serialize(Boolean debug) {
		String newMessage = new String();
		
		//own id (of the site who is sending the message)
		newMessage += this.ID.toString() + "|";
		
		//sender state id
		for(int i=0; i<state.size(); i++)
			newMessage += state.get(i).toString() + " ";
		
		newMessage += "|";
		
		//operation
		if (this.command != null)
			newMessage += this.command.Serialize();
		else
			newMessage += "EMPTY";

		newMessage += "|";
		
		//transformed operation
		if(debug == true) {
			if (this.transformedCommand != null)
				newMessage += this.transformedCommand.Serialize();
			else
				newMessage += "EMPTY";
			
			newMessage += "|";
		}
		
		//priority
		for(int i=0; i<this.priorityArray.size(); i++)
			newMessage += this.priorityArray.get(i) + " ";
		
		return newMessage;
	}
}
