public class Message {
	ABCASTMessage abmessage;
	CBCASTMessage cbmessage;
	Command c;

	synchronized String Serialize() {
		if (abmessage != null)
			return c.Serialize() + "+" + cbmessage.Serialize() + "+"
					+ abmessage.Serialize();
		else
			return c.Serialize() + "+" + cbmessage.Serialize() + "+-";
	}

	synchronized void Deserialize(String str) throws Exception {
		String[] tokens = str.split("\\+");
		if (tokens.length != 3)
			throw new Exception("Wrong command");

		c = new Command();
		c.Deserialize(tokens[0]);

		cbmessage = new CBCASTMessage();
		cbmessage.Deserialize(tokens[1]);
		if (!tokens[2].equals("-")) {
			abmessage = new ABCASTMessage();
			abmessage.Deserialize(tokens[2]);
		}
		else
			abmessage = null;
	}
}
