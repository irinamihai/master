public class TotalOrderMessage implements Comparable<TotalOrderMessage> {
	enum MessageType {
		REVISE_TS, PROPOSED_TS, FINAL_TS
	};

	MessageType messageType;
	String uid;
	Integer senderId;
	Integer timestamp;
	Boolean deliverable;
	Command command;

	synchronized public String Serialize() {
		String newMessage = new String();
		if (command != null)
			newMessage += command.Serialize();
		else
			newMessage += "EMPTY";

		newMessage += "+" + senderId;
		newMessage += "@" + uid;
		newMessage += "@" + timestamp;
		newMessage += "@" + messageType;

		return newMessage;
	}

	synchronized public void Deserialize(String currentString) throws Exception {

		String[] tokens = currentString.split("\\+");
		System.out.print(tokens[0] + "\n");
		// if(tokens[0].length() != 3)
		// throw new Exception("Wrong command");

		command = new Command();
		command.Deserialize(tokens[0]);

		String[] messageTokens = tokens[1].split("@");

		if (messageTokens[3].equalsIgnoreCase("REVISE_TS")) {
			messageType = MessageType.REVISE_TS;
			senderId = Integer.parseInt(messageTokens[0]);
			uid = messageTokens[1];
			timestamp = Integer.parseInt(messageTokens[2]);
			deliverable = false;
			return;
		} else if (messageTokens[3].equalsIgnoreCase("PROPOSED_TS"))
			messageType = MessageType.PROPOSED_TS;
		else
			messageType = MessageType.FINAL_TS;

		senderId = Integer.parseInt(messageTokens[0]);
		uid = messageTokens[1];
		timestamp = Integer.parseInt(messageTokens[2]);
		deliverable = false;
	}

	@Override
	public int compareTo(TotalOrderMessage arg0) {
		if (timestamp == arg0.timestamp)
			return uid.compareTo(arg0.uid);
		else
			return timestamp.compareTo(arg0.timestamp);
	}
}
