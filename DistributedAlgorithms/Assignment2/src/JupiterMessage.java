public class JupiterMessage {
	Integer ID;
	Integer myMsgs;
	Integer otherMsgs;
	Command c;

	JupiterMessage() {	
		c = new Command();
	}
	
	JupiterMessage(Command newCommand, Integer ID, Integer myMsg, Integer otherMsg) {
		
		c = new Command();
		c.character = newCommand.character;
		c.position = newCommand.position;
		c.type = newCommand.type;
		
		this.ID = ID;
		this.myMsgs = myMsg;
		this.otherMsgs = otherMsg;
	}
	
	synchronized public void Deserialize(String currentString) throws Exception {
		String[] tokens = currentString.split("\\|");
		
		//get sender id
		this.ID = Integer.parseInt(tokens[0]);
			
		//get mymsgs
		this.myMsgs = Integer.parseInt(tokens[1]);
		
		//get othermsgs
		this.otherMsgs = Integer.parseInt(tokens[2]);
		
		//get operation
		this.c.Deserialize(tokens[3]);
	}
	
	synchronized public String Serialize() {
		String newMessage = new String();
		
		newMessage += this.ID.toString();
		newMessage += "|";
		
		newMessage += this.myMsgs.toString();
		newMessage += "|";
		
		newMessage += this.otherMsgs.toString();
		newMessage += "|";
		
		newMessage += this.c.Serialize();		
		newMessage += "|";
		
		return newMessage;
	}
}
