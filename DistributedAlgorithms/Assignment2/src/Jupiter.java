import java.util.ArrayList;
import java.util.Iterator;

public class Jupiter extends Algorithm {
	Integer ID;
	Integer myMsgs; // generate de mine
	Integer otherMsgs; // primite de la altii
	ArrayList<JupiterMessage> outgoing;
	ArrayList<Integer> state_received;
	ArrayList<Integer> state_sent;
	Boolean server;

	Jupiter(int myport, Client[] clients, UIModule ui) {
		super(myport, clients, ui);

		myMsgs = 0;
		otherMsgs = 0;
		outgoing = new ArrayList<JupiterMessage>();
		state_received = new ArrayList<Integer>();
		state_sent = new ArrayList<Integer>();

		for (int i = 0; i < clients.length; i++) {
			state_received.add(0);
			state_sent.add(0);
		}

		ID = myport - 50000;

		if (ID == 0)
			server = true;
		else
			server = false;
	}

	@Override
	synchronized public void receiveMessage(String m, String uid) {

		JupiterMessage receivedMessage = new JupiterMessage();

		try {
			receivedMessage.Deserialize(m);
			if (receivedMessage.c.type.equals(Command.Type.EOF)
					|| receivedMessage.c.type.equals(Command.Type.EMPTY)) {
				ui.debug("[Recv]: " + m + ", discarded");
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		ui.debug("[Recv]: " + m);

		ui.debug("Queue before removal:");
		printQueue();
		// remove ack messages
		for (Iterator<JupiterMessage> iterator = outgoing.iterator(); iterator
				.hasNext();) {
			JupiterMessage msg = iterator.next();
			if (msg.myMsgs < receivedMessage.otherMsgs
					|| msg.c.type.equals(Command.Type.EMPTY))
				iterator.remove();
		}
		ui.debug("Queue after removal:");
		printQueue();

		// transform operations
		for (int i = 0; i < outgoing.size(); i++) {
			ui.debug("Initial recv:" + receivedMessage.c.Serialize());
			xform(receivedMessage, outgoing.get(i));
			ui.debug("After recv:" + receivedMessage.c.Serialize());
		}

		if (this.server == true
				&& !receivedMessage.c.type.equals(Command.Type.EMPTY))
			broadcast(receivedMessage, receivedMessage.ID, true, this.myMsgs,
					this.state_received, 1);

		// apply operation locally
		if (!receivedMessage.c.type.equals(Command.Type.EMPTY)) {
			ui.debug("Command type: " + receivedMessage.c.type);
			ui.debug("Exec.other" + receivedMessage.ID + ": "
					+ receivedMessage.Serialize());
			ui.executeCommand(receivedMessage.c);
		}

		// increment total number of messages received
		this.otherMsgs++;

		// increment number of messages received from the specific site
		this.state_received.set(receivedMessage.ID,
				this.state_received.get(receivedMessage.ID) + 1);
	}

	synchronized public void xform(JupiterMessage recv, JupiterMessage out) {

		transformationType transformationTypeNeeded = getTransformationType(
				recv.c, out.c);

		if (transformationTypeNeeded == null) {
			System.out.println("Null transform " + this.myport + ": "
					+ recv.c.type + " " + out.c.type);
			return;
		}
		/*
		 * operationTransformation transformation = new
		 * operationTransformation();
		 * 
		 * Command rcvCommandTransformed = new Command(); Command
		 * outCommandTransformed = new Command();
		 */
		switch (transformationTypeNeeded) {

		case DEL_DEL:
			ui.debug("DEL_DEL" + recv.c.Serialize() + " vs "
					+ out.c.Serialize());
			if (recv.c.position < out.c.position) {
				out.c.position--;
			} else if (recv.c.position > out.c.position) {
				recv.c.position--;
			} else {
				recv.c.type = Command.Type.EMPTY;
				out.c.type = Command.Type.EMPTY;
			}
			break;
		case INS_INS:
			ui.debug("INS_INS" + recv.c.Serialize() + " vs "
					+ out.c.Serialize());
			if (recv.c.position < out.c.position) {
				out.c.position++;
			} else if (recv.c.position > out.c.position) {
				recv.c.position++;
			} else if (recv.c.character != out.c.character) {
				if (recv.ID > out.ID)
					recv.c.position++;
				else
					out.c.position++;
			} else {
				recv.c.type = Command.Type.EMPTY;
				out.c.type = Command.Type.EMPTY;
			}
			break;
		case DEL_INS:
			ui.debug("DEL_INS" + recv.c.Serialize() + " vs "
					+ out.c.Serialize());
			if (recv.c.position < out.c.position) {
				out.c.position--;
			} else
				recv.c.position++;
			break;
		case INS_DEL:
			ui.debug("INS_DEL" + recv.c.Serialize() + " vs "
					+ out.c.Serialize());
			if (recv.c.position < out.c.position) {
				out.c.position++;
			} else
				recv.c.position--;
			break;
		}
		/*
		 * if (rcvCommandTransformed != null) { recv.c.character =
		 * rcvCommandTransformed.character; recv.c.position =
		 * rcvCommandTransformed.position; recv.c.type =
		 * rcvCommandTransformed.type; } // else // recv.c.type =
		 * Command.Type.EMPTY;
		 * 
		 * if (outCommandTransformed != null) { out.c.character =
		 * outCommandTransformed.character; out.c.position =
		 * outCommandTransformed.position; out.c.type =
		 * outCommandTransformed.type; } // else // out.c.type =
		 * Command.Type.EMPTY;
		 */
	}

	synchronized public transformationType getTransformationType(Command c1,
			Command c2) {

		if (c1.type.equals(Command.Type.Del)
				&& c2.type.equals(Command.Type.Del))
			return transformationType.DEL_DEL;

		if (c1.type.equals(Command.Type.Ins)
				&& c2.type.equals(Command.Type.Ins))
			return transformationType.INS_INS;

		if (c1.type.equals(Command.Type.Del)
				&& c2.type.equals(Command.Type.Ins))
			return transformationType.DEL_INS;

		if (c1.type.equals(Command.Type.Ins)
				&& c2.type.equals(Command.Type.Del))
			return transformationType.INS_DEL;

		return null;
	}

	@Override
	synchronized public Message sendCommand(Command c, Boolean bcast) {
		if (c.type.equals(Command.Type.EOF)
				|| c.type.equals(Command.Type.EMPTY)) {
			return null;
		}

		// if server, send to all other; if client, send only to server
		if (this.server == true) {
			ui.debug("Exec.my: " + c.Serialize());
			ui.executeCommand(c);
			JupiterMessage newMessage = new JupiterMessage(c, this.ID,
					this.myMsgs, this.otherMsgs);
			broadcast(newMessage, -1, false, this.myMsgs, this.state_received,
					0);
			outgoing.add(newMessage);
			ui.debug("New queue:");
			printQueue();
			this.myMsgs++;

		} else {
			ui.debug("Exec.my: " + c.Serialize());
			ui.executeCommand(c);
			JupiterMessage newMessage = new JupiterMessage(c, this.ID,
					this.myMsgs, this.otherMsgs);
			ui.debug("ToServer: " + newMessage.Serialize());
			sendToServer(newMessage);
			outgoing.add(newMessage);
			ui.debug("New queue:");
			printQueue();
			this.myMsgs++;
		}

		return null;
	}

	synchronized public void sendToServer(JupiterMessage newMessage) {
		final String str = newMessage.Serialize();
		clients[0].sendMessage(str);
	}

	synchronized public void incrementSentState(Integer site) {
		this.state_sent.set(site, state_sent.get(site) + 1);
	}

	synchronized public void broadcast(final JupiterMessage msg,
			final Integer client, final Boolean noClient,
			final Integer myMessages, final ArrayList<Integer> otherMessages,
			final Integer transfOper) {
		// final String str = msg.Serialize();

		for (int i = 0; i < clients.length; i++) {
			if (noClient == true && i == client)
				continue;

			if (clients[i].getPort() != myport) {
				final int clientIndex = i;
				JupiterMessage newMsg = new JupiterMessage(msg.c, msg.ID,
						myMessages, otherMessages.get(clientIndex));
				String str = newMsg.Serialize();
				ui.debug("[" + transfOper.toString() + "Broadcast:"
						+ clientIndex + "] " + str);
				clients[clientIndex].sendMessage(str);
				incrementSentState(clientIndex);

			}
		}
	}

	synchronized void printQueue() {
		for (int i = 0; i < this.outgoing.size(); i++)
			ui.debug(this.outgoing.get(i).Serialize());
	}

}