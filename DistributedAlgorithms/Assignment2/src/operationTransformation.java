import java.util.ArrayList;

public class operationTransformation {
	
	/* DOPT Transformation */
	synchronized public Command deleteDeleteTransformation(Command Oi,
			Command Oj, ArrayList<Integer> pi, ArrayList<Integer> pj) {

		Command newCommand = new Command();
		newCommand.type = Oi.type;
		newCommand.character = Oi.character;
		newCommand.position = Oi.position;
		
		if (Oi.position < Oj.position) {
			return newCommand;
		}
		else if (Oi.position > Oj.position) {
			newCommand.position--;
			return newCommand;
		} else {
			newCommand.type = Command.Type.EMPTY;
			return newCommand;
		}

		// return null;
	}

	synchronized public Command insertInsertTransformation(Command Oi,
			Command Oj, ArrayList<Integer> pi, ArrayList<Integer> pj) {

		Command newCommand = new Command();
		newCommand.type = Oi.type;
		newCommand.character = Oi.character;
		newCommand.position = Oi.position;
		
		if (Oi.position < Oj.position) {
			return newCommand;
		}
		else if (Oi.position > Oj.position) {
			newCommand.position++;
			return newCommand;
		} else {
			if (Oi.character == Oj.character) {
				newCommand.type = Command.Type.EMPTY;
				return newCommand;
			}
			else {
				if (pi.size() > pj.size()) {
					newCommand.position = Oi.position++;
					return newCommand;
				} else {
					return newCommand;
				}
			}
		}
	}

	synchronized public Command deleteInsertTransformation(Command Oi,
			Command Oj, ArrayList<Integer> pi, ArrayList<Integer> pj) {

		Command newCommand = new Command();
		newCommand.type = Oi.type;
		newCommand.character = Oi.character;
		newCommand.position = Oi.position;
		
		if (Oi.position < Oj.position){
			return newCommand;
		}
		else {
			newCommand.position++;
			return newCommand;
		}

	}

	synchronized public Command insertDeleteTransformation(Command Oi,
			Command Oj, ArrayList<Integer> pi, ArrayList<Integer> pj) {

		Command newCommand = new Command();
		newCommand.type = Oi.type;
		newCommand.character = Oi.character;
		newCommand.position = Oi.position;
		
		if (Oi.position < Oj.position) {
			return newCommand;	
		}
		else {
			newCommand.character = Oi.character;
			newCommand.position = Oi.position--;
			return newCommand;	
		}
	}

}
