import java.util.Random;

public abstract class Algorithm {
	protected final int myport;
	protected final Client[] clients;
	protected final UIModule ui;

	public abstract void receiveMessage(String m, String uid);

	public abstract Message sendCommand(Command c, Boolean bcast);

	public Algorithm(int myport, Client[] clients, UIModule ui) {
		this.clients = clients;
		this.ui = ui;
		this.myport = myport;
	}

	synchronized public void broadcast(Message msg) {
		final String str = msg.Serialize();
		final Random rand = new Random();

		ui.debug("[Broadcast] " + str);

		for (int i = 0; i < clients.length; i++) {
			if (clients[i].getPort() == myport) {
				clients[i].sendMessage(str);
			} else {
				final int clientIndex = i;
				final Thread thr = new Thread(new Runnable() {
					public void run() {
						try {
							Thread.sleep(rand.nextInt(401) + 100);
							clients[clientIndex].sendMessage(str);
						} catch (Exception e) {
						}
					}
				});
				thr.start();
			}
		}
	}
}
