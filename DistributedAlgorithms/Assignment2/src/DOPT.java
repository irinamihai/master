import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

enum stateVectorRelation {
	EQUAL, SMALLER, GREATER
};

enum transformationType {
	DEL_DEL, INS_INS, DEL_INS, INS_DEL
};

public class DOPT extends Algorithm {
	LinkedList<DOPTMessage> requestQueue;
	LinkedList<DOPTMessage> logQueue;
	ArrayList<Integer> state;
	Integer ID;
	public ArrayList<Integer> priorityArray;
	Integer eofNumber;

	public DOPT(int myport, Client[] clients, UIModule ui) {
		super(myport, clients, ui);
		requestQueue = new LinkedList<DOPTMessage>();
		logQueue = new LinkedList<DOPTMessage>();
		// state = new HashMap<>();
		state = new ArrayList<Integer>();
		ID = this.myport - 50000;
		System.out.println("Myport: " + myport);
		for (int i = 0; i < clients.length; i++)
			state.add(i, 0);

		priorityArray = new ArrayList<Integer>();
		eofNumber = 0;
	}

	synchronized public void printRequestQueue() {
		ui.debug("Request Queue:");
		for (int i = 0; i < requestQueue.size(); i++)
			ui.debug(i + ": " + requestQueue.get(i).Serialize(false));
	}

	synchronized public void printLogQueue() {
		ui.debug("Log Queue:");
		for (int i = 0; i < logQueue.size(); i++)
			ui.debug(i + ": " + logQueue.get(i).Serialize(true));
	}

	@Override
	synchronized public void receiveMessage(String m, String uid) {

		ui.debug("[Rec:]" + m);
		DOPTMessage receivedMessage = new DOPTMessage();

		try {
			receivedMessage.Deserialize(m);
			// add message to request queue if different than EOF and is not
			// from myself
			if (!receivedMessage.command.type.equals(Command.Type.EOF)
					&& receivedMessage.ID != this.ID)
				requestQueue.addFirst(receivedMessage);

			// count number of received EOFs and when it is equal with the
			// number of sites, empty request queue
			ui.debug("Command type: " + receivedMessage.command.type + "");

			if (receivedMessage.command.type.equals(Command.Type.EOF)) {
				eofNumber++;
				ui.debug("EOF number: " + eofNumber.toString());
			}

			if (eofNumber == clients.length) {
				ui.debug("Empty request queue");

				if (receivedMessage.command.type.equals(Command.Type.EOF)) {
					ui.debug("Left in request queue: ");
					String ss = " ";
					for (int i = 0; i < requestQueue.size(); i++) {
						if (requestQueue.get(i).command
								.equals(Command.Type.EMPTY))
							ss += "null ";
						else
							ss += requestQueue.get(i).command.Serialize() + " ";
					}

					ui.debug(ss);
				}
			}

			printRequestQueue();

			// check the request queue for operations ready to be delivered
			checkOperationExecution();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	synchronized public void checkOperationExecution() {
		Boolean deliver = true;
		while (deliver) {
			deliver = false;

			for (Iterator<DOPTMessage> iterator = requestQueue.iterator(); iterator
					.hasNext();) {
				DOPTMessage remoteRequest = iterator.next();
				stateVectorRelation temp_state = compareStateVectors(
						remoteRequest.state, this.state);

				switch (temp_state) {

				case EQUAL:
					ui.debug("States: " + serializeState(this.state) + " = "
							+ serializeState(remoteRequest.state) + "("
							+ remoteRequest.ID + ")");

					// execute immeditately, without transformation
					ui.executeCommand(remoteRequest.command);

					ui.debug("[Exec]" + remoteRequest.command.Serialize());

					// add to log queue
					DOPTMessage newLogMessage = new DOPTMessage();
					newLogMessage.command = remoteRequest.command;
					newLogMessage.transformedCommand = new Command();
					newLogMessage.transformedCommand.character = remoteRequest.command.character;
					newLogMessage.transformedCommand.position = remoteRequest.command.position;
					newLogMessage.transformedCommand.type = remoteRequest.command.type;

					newLogMessage.ID = remoteRequest.ID;
					newLogMessage.priorityArray = new ArrayList<Integer>(
							remoteRequest.priorityArray);
					newLogMessage.state = new ArrayList<Integer>(this.state);

					printLogQueue();
					ui.debug("To log: " + newLogMessage.Serialize(true));
					logQueue.addFirst(newLogMessage);
					printLogQueue();

					ui.debug("Before: " + serializeState(remoteRequest.state));
					// update current state vector
					state.set(remoteRequest.ID, state.get(remoteRequest.ID) + 1);
					ui.debug("After: " + serializeState(remoteRequest.state));

					printRequestQueue();
					// remove from request queue
					iterator.remove();
					printRequestQueue();

					printLogQueue();
					deliver = true;
					break;
				case GREATER:
					System.out.println("No execution at site " + this.ID);
					break;
				case SMALLER:
					/*
					 * current site has executed more operations that the
					 * sending site
					 */
					ui.debug(serializeState(this.state) + " > "
							+ serializeState(remoteRequest.state));

					remoteRequest.transformedCommand = new Command();
					remoteRequest.transformedCommand.character = remoteRequest.command.character;
					remoteRequest.transformedCommand.position = remoteRequest.command.position;
					remoteRequest.transformedCommand.type = remoteRequest.command.type;

					// search for the most recent entry in log queue, if there
					// is a log
					this.printLogQueue();
					DOPTMessage mostRecentLog = getMostRecentLog(
							remoteRequest.state, 0);

					ui.debug("Recent: " + mostRecentLog.Serialize(true));

					// while finding states in the log
					while (mostRecentLog != null) {

						printLogQueue();
						ui.debug("First most recent: "
								+ mostRecentLog.Serialize(true));

						// let k be the sender for mostRecentLog operation
						// if value for k, from the remote request's < value for
						// k from mostRecentLog state
						if (remoteRequest.state.get(mostRecentLog.ID) <= mostRecentLog.state
								.get(mostRecentLog.ID)) {
							// transform operation
							ui.debug("Transform "
									+ remoteRequest.command.Serialize()
									+ " with "
									+ mostRecentLog.command.Serialize());

							transformationType transformationTypeNeeded = getTransformationType(
									remoteRequest.transformedCommand,
									mostRecentLog.command);

							if (transformationTypeNeeded == null) {
								mostRecentLog = getMostRecentLog(
										remoteRequest.state,
										logQueue.indexOf(mostRecentLog) + 1);
								continue;
							}

							remoteRequest.transformedCommand = executeOperationTransformation(
									transformationTypeNeeded, mostRecentLog,
									remoteRequest);

							if (!remoteRequest.transformedCommand
									.equals(Command.Type.EMPTY))
								ui.debug("Results: "
										+ remoteRequest.transformedCommand
												.Serialize());
							else
								ui.debug("Results: " + null);
						}

						Integer formerPosition = logQueue
								.indexOf(mostRecentLog);
						ui.debug("Former Position: " + formerPosition);
						mostRecentLog = getMostRecentLog(remoteRequest.state,
								formerPosition + 1);
					}

					if (!remoteRequest.command.equals(Command.Type.EMPTY)) {
						if (!remoteRequest.transformedCommand
								.equals(Command.Type.EMPTY)) {
							ui.executeCommand(remoteRequest.transformedCommand);
							ui.debug("[Exec] "
									+ remoteRequest.transformedCommand
											.Serialize());
						}

						newLogMessage = new DOPTMessage();
						newLogMessage.command = remoteRequest.command;
						newLogMessage.transformedCommand = remoteRequest.transformedCommand;
						newLogMessage.ID = remoteRequest.ID;
						newLogMessage.priorityArray = new ArrayList<Integer>();
						newLogMessage.priorityArray
								.addAll(remoteRequest.priorityArray);
						newLogMessage.state = new ArrayList<Integer>(
								remoteRequest.state);

						logQueue.addFirst(newLogMessage);

						ui.debug("Before: " + serializeState(this.state));
						this.state.set(remoteRequest.ID,
								this.state.get(remoteRequest.ID) + 1);
						ui.debug("After: " + serializeState(this.state));

						iterator.remove();
						deliver = true;
					}
					break;
				}
			}
		}
	}

	synchronized public Command executeOperationTransformation(
			transformationType transformationTypeNeeded, DOPTMessage Oj,
			DOPTMessage Ok) {

		operationTransformation transformation = new operationTransformation();
		Command newCommand = new Command();

		switch (transformationTypeNeeded) {

		case DEL_DEL:
			newCommand = transformation.deleteDeleteTransformation(Ok.command,
					Oj.command, Ok.priorityArray, Oj.priorityArray);
			break;
		case INS_INS:
			newCommand = transformation.insertInsertTransformation(Ok.command,
					Oj.command, Ok.priorityArray, Oj.priorityArray);
			break;
		case DEL_INS:
			newCommand = transformation.deleteInsertTransformation(Ok.command,
					Oj.command, Ok.priorityArray, Oj.priorityArray);
			break;
		case INS_DEL:
			newCommand = transformation.insertDeleteTransformation(Ok.command,
					Oj.command, Ok.priorityArray, Oj.priorityArray);
			break;
		}

		return newCommand;
	}

	synchronized public transformationType getTransformationType(
			Command command1, Command command2) {

		if (command1.type.equals(Command.Type.Del)
				&& command2.type.equals(Command.Type.Del))
			return transformationType.DEL_DEL;

		if (command1.type.equals(Command.Type.Ins)
				&& command2.type.equals(Command.Type.Ins))
			return transformationType.INS_INS;

		if (command1.type.equals(Command.Type.Del)
				&& command2.type.equals(Command.Type.Ins))
			return transformationType.DEL_INS;

		if (command1.type.equals(Command.Type.Ins)
				&& command2.type.equals(Command.Type.Del))
			return transformationType.INS_DEL;

		return null;
	}

	synchronized public DOPTMessage getMostRecentLog(
			ArrayList<Integer> remoteState, Integer formerPosition) {

		// begin from the position of the most recent log previously found
		if (formerPosition == 0) {
			for (int i = formerPosition; i < logQueue.size(); i++) {
				stateVectorRelation currentRelation = compareStateVectors(
						logQueue.get(i).state, remoteState);

				if (currentRelation.equals(stateVectorRelation.SMALLER)
						|| currentRelation.equals(stateVectorRelation.EQUAL))
					return logQueue.get(i);
			}
		} else if (formerPosition < logQueue.size())
			return logQueue.get(formerPosition);

		return null;
	}

	synchronized private stateVectorRelation compareStateVectors(
			ArrayList<Integer> currentState, ArrayList<Integer> remoteState) {
		int smaller = 0;

		for (int i = 0; i < currentState.size(); i++) {
			// if at least one component is greater, return greater
			if (currentState.get(i) > remoteState.get(i))
				return stateVectorRelation.GREATER;

			// if is smaller, count it
			if (currentState.get(i) < remoteState.get(i))
				smaller++;
		}

		if (smaller == 0)
			return stateVectorRelation.EQUAL;
		else
			return stateVectorRelation.SMALLER;
	}

	public stateVectorRelation comparePriorities(ArrayList<Integer> priority1,
			ArrayList<Integer> priority2) {

		if (priority1.size() - priority2.size() < 0)
			return stateVectorRelation.SMALLER;
		else if (priority1.size() - priority2.size() > 0)
			return stateVectorRelation.GREATER;
		else {
			for (int i = 0; i < priority1.size(); i++) {
				if (priority1.get(i) < priority2.get(i))
					return stateVectorRelation.SMALLER;
				if (priority1.get(i) > priority2.get(i))
					return stateVectorRelation.GREATER;
			}
		}

		return stateVectorRelation.EQUAL;
	}

	private synchronized ArrayList<Integer> findHighestPriority(Command command) {
		ArrayList<Integer> highestPriority = new ArrayList<Integer>();

		for (int i = logQueue.size() - 1; i >= 0; i--) {

			// if positions differ, nothing to do for this index
			if (logQueue.get(i).command.position != command.position)
				continue;

			// if highest position is still empty when we get here, assign it
			// the current value from the log
			if (highestPriority.isEmpty()) {
				highestPriority.addAll(logQueue.get(i).priorityArray);
				continue;
			}

			if (comparePriorities(highestPriority,
					logQueue.get(i).priorityArray).equals(
					stateVectorRelation.SMALLER)) {
				highestPriority.clear();
				highestPriority.addAll(logQueue.get(i).priorityArray);
				break;
			}
		}

		return highestPriority;
	}

	@Override
	synchronized public Message sendCommand(Command c, Boolean bcast) {

		// create new originating message
		DOPTMessage newMessage = new DOPTMessage();
		newMessage.command = c;
		newMessage.transformedCommand = c;
		newMessage.ID = this.ID;
		newMessage.state = this.state;
		newMessage.priorityArray = new ArrayList<Integer>();

		// get message priority
		ArrayList<Integer> currentPriority = findHighestPriority(c);

		// if no highest priority found, set the priority to ID,
		// else add the ID after updating the current priority
		if (!currentPriority.isEmpty())
			newMessage.priorityArray.addAll(currentPriority);

		newMessage.priorityArray.add(this.ID);

		// add new message to the request queue only if different than EOF
		if (!newMessage.command.type.equals(Command.Type.EOF))
			requestQueue.addFirst(newMessage);

		// bcast message
		broadcast(newMessage);

		return null;
	}

	synchronized public void broadcast(DOPTMessage msg) {

		final String str = msg.Serialize(false);
		final Random rand = new Random();

		ui.debug("[Bcast] " + str);

		for (int i = 0; i < clients.length; i++) {
			if (clients[i].getPort() == myport) {
				clients[i].sendMessage(str);
			} else {
				final int clientIndex = i;
				final Thread thr = new Thread(new Runnable() {
					public void run() {
						try {
							Thread.sleep(rand.nextInt(401) + 100);
							clients[clientIndex].sendMessage(str);
						} catch (Exception e) {
						}
					}
				});
				thr.start();
			}
		}
	}

	synchronized String serializeState(ArrayList<Integer> someState) {
		String s1 = "";
		for (int j = 0; j < someState.size(); j++)
			s1 += someState.get(j).toString() + " ";

		return s1;
	}
}
