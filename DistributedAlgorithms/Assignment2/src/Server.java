import java.net.*;
import java.io.*;

public class Server extends Thread {
	private ServerSocket serverSocket;
	public Algorithm algorithm;
	private int numberOfClients;
	private Thread[] readers;

	public Server(int port, int numberOfClients) throws IOException {
		serverSocket = new ServerSocket(port);
		serverSocket.setSoTimeout(10000);
		this.numberOfClients = numberOfClients;
		readers = new Thread[numberOfClients];
	}

	public void run() {
		for (int i = 0; i < numberOfClients; i++) {
			readers[i] = new Thread(new Runnable() {
				public void run() {
					try {
						Socket server = serverSocket.accept();
						DataInputStream in = new DataInputStream(server.getInputStream());
						while (true) {
							algorithm.receiveMessage(in.readUTF(), "");
						}
					} catch (EOFException e) {
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			readers[i].start();
		}
	}
}
