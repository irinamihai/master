import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Random;

public class Driver {
	private final Algorithm algorithm;
	private final UIModule ui;

	public Driver(Algorithm algorithm, UIModule ui) {
		this.algorithm = algorithm;
		this.ui = ui;
	}

	public void readCommandsFrom(String fileName) throws Exception {
		ui.debug("Reading commands");
		final Random rand = new Random();
		final BufferedReader readerObject = new BufferedReader(new FileReader(fileName));
		try {
			String currentCommand;
			while ((currentCommand = readerObject.readLine()) != null) {
				Command C = new Command();
				if (C.decodeFromFile(currentCommand) == false) {
					continue;
				}
				Thread.sleep(300);
				algorithm.sendCommand(C, true);
			}
			final Command C = new Command();
			C.type = Command.Type.EOF;
			Thread.sleep(rand.nextInt(101) + 100);
			algorithm.sendCommand(C, true);
		} finally {
			readerObject.close();
		}
	}
}
