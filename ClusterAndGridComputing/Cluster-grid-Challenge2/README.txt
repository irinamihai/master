Accesul la masina (parola: student):
ssh root@10.9.4.32

Elementele folosite pentru monitorizare sunt in /root/monitor:
	- Sursa pentru prelucrarea matricilor se afla in main.cpp, compilat cu
	 g++ main.cpp -lopencv_core -lopencv_highgui -lblas -lnetcdf
	- Scriptul de monitorizare este monitor.sh
	
Datele au fost injectate in graphite si graficele obtinute sunt in:
	- folderul torque pentru rularea cu qsub
	- folderul mosix pentru rularea cu mosrun
	

Rezultatele au fost oarecum de asteptat pentru ambele rulari:
	- in cazul memoriei se observa o crestere la inceputul rularii programului care ramane constanta pana la sfarsitul programului
	- in cazul ioread se observa un spike mic la inceput - citirea primei imagini si apoi, atat pentru ioread, cat si pentru iowrite se observa un spike mai mare, care, din ce am observat, corespunde momentului in care se realizeaza scrieri pe disc.
	- in cazul CPU-ul am observat un comportament diferit pentru torque - mosix
		TORQUE - CPU-ul urca in aprox 80-90% si ramane acolo pana la sfarsitul executiei programului
		MOSIX  - CPU-ul variaza intre 45% si 65% cu spike-uri dese
