#!/bin/sh

cd /root/monitor
./a.out &

interval=0
PORT=2003
SERVER=10.9.3.212

while [ $interval -lt 1200 ]; do
   interval=$((interval+1))
   cpu=$(top -bi -n 1 | egrep "a.out" | tr -s ' ' | sed -e 's/^[ \t]*//' | cut -f 9 -d ' ')
   memory=$(top -bi -n 1 | egrep "a.out" | tr -s ' ' | sed -e 's/^[ \t]*//' | cut -f 10 -d ' ')
   kb_read=$(iostat |  tr -s ' ' | sed -e 's/^[ \t]*//' | grep "vda" | cut -f 3 -d ' ')
   kb_write=$(iostat |  tr -s ' ' |  sed -e 's/^[ \t]*//' | egrep "vda" | cut -f 4 -d ' ')

   echo "CPU: "$cpu" Memory: "$memory" kB read: "$kb_read" kB write: "$kb_write

   echo "tema2.imihai.mosix.cpu $cpu `date +%s`" | nc ${SERVER} ${PORT}
   echo "tema2.imihai.mosix.memory $memory `date +%s`" | nc ${SERVER} ${PORT}
   echo "tema2.imihai.mosix.ioread $kb_read `date +%s`" | nc ${SERVER} ${PORT}
   echo "tema2.imihai.mosix.iowrite $kb_write `date +%s`" | nc ${SERVER} ${PORT}

   sleep 1
done
