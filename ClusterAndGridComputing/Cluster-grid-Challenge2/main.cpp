#include <iostream>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <netcdf.h>

using namespace std;
using namespace cv;

#define integer int
#define doublereal double
extern "C" {
   int dgemm_(char *transa, char *transb, integer *m, integer *
        n, integer *k, doublereal *alpha, doublereal *a, integer *lda,
        doublereal *b, integer *ldb, doublereal *beta, doublereal *c__,
        integer *ldc);
}

/* Handle errors by printing an error message and exiting with a
 * non-zero status. */
#define ERRCODE 2
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE);}

int main() {
    ifstream imageFile("file.in");
    if(!imageFile.is_open()) {
        cout<<"Error at reading images names\n";
        return 1;
    }

    while(!imageFile.eof()) {
        string imageName;
        imageFile>>imageName;
        cout<<"Reading image "<<imageName<<endl;
        Mat diskimage = imread(imageName, CV_LOAD_IMAGE_GRAYSCALE);
        Mat image;
        diskimage.convertTo(image, CV_64F);
        if(!image.data) {
            cout<<"Could not read image "<<imageName<<endl;
            return 1;
        }

        Mat Asquared(5000, 5000, CV_64F);
        //3*A^3
        char transA = 'n';
        int m = 5000, n = 5000, k = 5000;
        double alpha = 1;
        double alpha3 = 3;
        int lda = 5000;
        double beta = 0;
        double beta5 = 5;

        cout << "Computing A squared ... " << flush;
        //A*A
        dgemm_(&transA, &transA, &m, &n, &k, &alpha, (double*)image.data, &lda, (double*)image.data, &lda, &beta, (double*)Asquared.data, &lda);

        //3*A*(A*A) + 5*A
        cout << "Done" << endl << "Computing 3*A*A^2+5*A ... " << flush;
        dgemm_(&transA, &transA, &m, &n, &k, &alpha3, (double*)image.data, &lda, (double*)Asquared.data, &lda, &beta5, (double*)image.data, &lda);

        cout << "Done" << endl << "Writing " << imageName + ".cdf ..." << flush;

   /* We are writing 2D data, a 5000 x 5000 grid. */
   #define NDIMS 2
   #define NX 5000
   #define NY 5000

   /* When we create netCDF variables and dimensions, we get back an
    * ID for each one. */
   int ncid, x_dimid, y_dimid, varid;
   int dimids[NDIMS];

   /* This is the data array we will write. It will just be filled
    * with a progression of numbers for this example. */
   double * data_out = (double*)image.data;

   /* Loop indexes, and error handling. */
   int x, y, retval;

   /* Create the file. The NC_CLOBBER parameter tells netCDF to
    * overwrite this file, if it already exists.*/
   if ((retval = nc_create((imageName + ".nc").c_str(), NC_CLOBBER, &ncid)))
      ERR(retval);

   /* Define the dimensions. NetCDF will hand back an ID for each. */
   if ((retval = nc_def_dim(ncid, "x", NX, &x_dimid)))
      ERR(retval);
   if ((retval = nc_def_dim(ncid, "y", NY, &y_dimid)))
      ERR(retval);

   /* The dimids array is used to pass the IDs of the dimensions of
    * the variable. */
   dimids[0] = x_dimid;
   dimids[1] = y_dimid;

  /* Define the variable. The type of the variable in this case is
    * NC_DOUBLE (8-byte double). */
   if ((retval = nc_def_var(ncid, "data", NC_DOUBLE, NDIMS, dimids, &varid)))
      ERR(retval);

  /* End define mode. This tells netCDF we are done defining
    * metadata. */
   if ((retval = nc_enddef(ncid)))u
      ERR(retval);

   /* Write the data to the file. Although netCDF supports
    * reading and writing subsets of data, in this case we write all
    * the data in one operation. */
   if ((retval = nc_put_var_double(ncid, varid, data_out)))
      ERR(retval);

   /* Close the file. This frees up any internal netCDF resources
    * associated with the file, and flushes any buffers. */
   if ((retval = nc_close(ncid)))
      ERR(retval);

   cout << "Success!" << endl;

   }

    return 0;
}