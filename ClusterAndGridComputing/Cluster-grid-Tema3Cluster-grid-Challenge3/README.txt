Toate fisierele de mai jos se afla in folderul sources.

Codul sursa pentru operatiile cu matrici se afla in main.cpp.

Scripturile running_opteron.sh si running_nehalem.sh contin comanda de compilare pentru fiecare arhitectura si scriptul efectiv de monitorizare (pentru CPU si memorie) care injecteaza datele in graphite. Submit.sh foloseste qsub pentru a trimite scripturile anterioare in cozile corespunzatoare.

Graficele obtinute se afla in folderul images.
- CPU: procesorul sta ocupat >98%. Fiind rulate in acelasi timp, utilizarile CPU pentru Nehalem si Opteron par corelate, urmand aceleasi valori.
- Memory: Se afiseaza procentul de memorie utilizat din memoria totala (~2.75% din 16GB pe Opteron si ~1.25% din 32GB pe Nehalem)

Acest folder mai contine inca o imagine, tema3_nehalem_opteron_computed in care au fost injectate datele generate de scriptul watch.sh, adica numarul de imagini prelucrate de fiecare nod in parte la un anumit moment de timp.
- Se observa ca Nehalem are putere de calcul mai mare decat Opteron in cazul inmultirii de matrici cu Blas, operand mai multe imagini decat Opteron in acelasi numar de minute.
