#!/bin/sh

interval=0
PORT=2003
SERVER=10.9.3.212

while [ $interval -lt 1200 ]; do

    interval=$((interval+1))

    number_nehalem=`cat running-nehalem.sh.o* | grep Done | wc -l`
    number_opteron=`cat running-opteron.sh.o*  | grep Done | wc -l`

    echo "tema3.imihai.nehalem.computed $number_nehalem `date +%s`" | nc ${SERVER} ${PORT}
    echo "tema3.imihai.opteron.computed $number_opteron `date +%s`" | nc ${SERVER} ${PORT}

    sleep 1
done
