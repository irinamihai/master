#!/bin/bash

qsub -q ibm-nehalem.q -pe openmpi*1 1 -cwd -j y running-nehalem.sh
qsub -q ibm-opteron.q -pe openmpi*1 1 -cwd -j y running-opteron.sh

