#include <iostream>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

#define integer int
#define doublereal double
extern "C" {
   #include <cblas.h> 
/*   int cblas_dgemm(char *transa, char *transb, integer *m, integer *
	n, integer *k, doublereal *alpha, doublereal *a, integer *lda, 
	doublereal *b, integer *ldb, doublereal *beta, doublereal *c__, 
	integer *ldc);
*/
   void cblas_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc);
}

/* Handle errors by printing an error message and exiting with a
 * non-zero status. */
#define ERRCODE 2
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE);}

int main() {
    cout << "Beginning computing..." << endl;
    ifstream imageFile("file.in");
    if(!imageFile.is_open()) {
        cout<<"Error at reading images names\n";
        return 1;
    }

    cout << "No input file" << endl;

    while(!imageFile.eof()) {
        string imageName;
        imageFile >> imageName;
        cout << "Reading image " << imageName<<endl;
        Mat diskimage = imread(imageName, CV_LOAD_IMAGE_GRAYSCALE);
	Mat image;
	diskimage.convertTo(image, CV_64F);
        if(!image.data) {
            cout<<"Could not read image "<<imageName<<endl;
            return 1;
        }

        Mat Asquared(5000, 5000, CV_64F);
        //3*A^3
        char transA = 'n';
        int m = 5000, n = 5000, k = 5000;
        double alpha = 1;
        double alpha3 = 3;
        int lda = 5000;
        double beta = 0;
        double beta5 = 5;

        cout << "Computing A squared ... " << flush;
        //A*A
        //cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, alpha, A, lda, A, lda, beta, B, lda);
        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, alpha, (double*)image.data, lda, (double*)image.data, lda, beta, (double*)Asquared.data, lda);

        //3*A*(A*A) + 5*A
        cout << "Done" << endl << "Computing 3*A*A^2+5*A ... " << flush;
        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, alpha3, (double*)image.data, lda, (double*)Asquared.data, lda, beta5, (double*)image.data, lda);

        cout << "Done" << endl << flush;


   }

    return 0;
}
