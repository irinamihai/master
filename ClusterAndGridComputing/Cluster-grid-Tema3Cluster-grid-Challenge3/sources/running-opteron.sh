#!/bin/bash

module load libraries/atlas-3.10.1-gcc-4.4.6-opteron
module load libraries/opencv-2.4.1-gcc-4.4.6

g++ main.cpp -lcblas -latlas -lopencv_core -lopencv_highgui -I/opt/tools/libraries/atlas/3.10.1-opteron-gcc-4.4.6/include -L/opt/tools/libraries/atlas/3.10.1-opteron-gcc-4.4.6/lib -I/opt/tools/libraries/opencv/2.4.1-gcc-4.4.6/include -L/opt/tools/libraries/opencv/2.4.1-gcc-4.4.6/lib -o opteron.out

./opteron.out &

interval=0
PORT=2003
SERVER=10.9.3.212

while [ $interval -lt 1200 ]; do
   interval=$((interval+1))
   cpu=$(top -bi -n 1 | egrep "opteron.out" | tr -s ' ' | sed -e 's/^[ \t]*//' | cut -f 9 -d ' ')
   memory=$(top -bi -n 1 | egrep "opteron.out" | tr -s ' ' | sed -e 's/^[ \t]*//' | cut -f 10 -d ' ')

   echo "CPU: "$cpu" Memory: "$memory

   echo "tema3.imihai.opteron.cpu $cpu `date +%s`" | nc ${SERVER} ${PORT}
   echo "tema3.imihai.opteron.memory $memory `date +%s`" | nc ${SERVER} ${PORT}

   sleep 1
done
