import sys
import base64
import struct
import threading

def isqrt(x):
    if x < 0:
        raise ValueError('square root not defined for negative numbers')
    n = int(x)
    if n == 0:
        return 0
    a, b = divmod(n.bit_length(), 2)
    x = 2**(a+b)
    while True:
        y = (x + n//x)//2
        if y >= x:
            return x
        x = y

# called by each thread
def verify_thread(n, start, end):
	if (start%2 == 0):
		start = start - 1
	if (end%2 == 0):
		end = end + 1
	#print(str(start) + ", " + str(end))
	i = start
	percent = (end-start)//10
	threshold = start + percent
	num_passes = 0
	while i <= end:
		if n % i == 0:
			print("FOUND! p = " + str(i) + ", q = " + str(n//i))
			break
		i += 2
		if i>threshold:
			num_passes = num_passes + 1
			threshold = threshold + percent
			print(str(num_passes*10) + "%")
	
# get the second field from the public key file.
keydata = base64.b64decode(bytes(open('task3_key.txt').read().split(None)[1], 'utf-8'))

parts = []
while keydata:
    # read the length of the data
    dlen = struct.unpack('>I', keydata[:4])[0]

    # read in <length> bytes
    data, keydata = keydata[4:dlen+4], keydata[4+dlen:]

    parts.append(data)

E = eval('0x' + ''.join(['%02X' % x for x in parts[1]]))
N = eval('0x' + ''.join(['%02X' % x for x in parts[2]]))

print("E = " + str(E))
print("N = " + str(N))
A = isqrt(6*N)+1
x = isqrt(A**2 - 6*N)
print("A = " + str(A))
print("x = " + str(x))

p = (A - x) // 3
q = (A + x) // 2

print("P = " + str(p))
print("Q = " + str(q))

# Verifying around guessed p
Rstart = p - 200000000
Rend = p + 200000000
Num_Threads = 1

verify_thread(N, Rstart, Rend)

exit(0)

Quanta = (Rend-Rstart)/Num_Threads
threads = []
for i in range(0, Num_Threads):
	MyStart = Rstart + Quanta*i
	if(i<Num_Threads-1):
		MyEnd = MyStart + Quanta*(i+1)
	else:
		MyEnd = Rend
	t = threading.Thread(target=verify_thread, args=(N, int(MyStart), int(MyEnd)))
	threads.append(t)
	t.start()
	
for t in threads:
	t.join()
