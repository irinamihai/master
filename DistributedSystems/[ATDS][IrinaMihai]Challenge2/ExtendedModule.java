package net.floodlightcontroller.atds;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.projectfloodlight.openflow.protocol.OFFlowMod;
import org.projectfloodlight.openflow.protocol.OFPacketIn;
import org.projectfloodlight.openflow.protocol.OFPacketOut;
import org.projectfloodlight.openflow.protocol.OFVersion;
import org.projectfloodlight.openflow.protocol.action.OFAction;
import org.projectfloodlight.openflow.protocol.match.Match;
import org.projectfloodlight.openflow.protocol.match.MatchField;
import org.projectfloodlight.openflow.types.EthType;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.projectfloodlight.openflow.types.IpProtocol;
import org.projectfloodlight.openflow.types.MacAddress;
import org.projectfloodlight.openflow.types.OFBufferId;
import org.projectfloodlight.openflow.types.OFPort;
import org.projectfloodlight.openflow.types.TransportPort;
import org.projectfloodlight.openflow.types.U64;
import org.projectfloodlight.openflow.types.VlanVid;

import com.google.common.primitives.UnsignedInteger;
import com.kenai.jaffl.struct.Struct.Unsigned32;
import com.kenai.jaffl.struct.Struct.Unsigned64;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.packet.ARP;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.packet.TCP;

public class ExtendedModule extends BasicModule{
	long last_query_time = 0;
    String h2_IP = "192.168.56.114";
    String h3_IP = "192.168.56.115";

	@Override
	protected void pseudoConstructor() {
		// TODO Auto-generated method stub
		
	}
	
    @Override
    protected void receivePacketIn(IOFSwitch sw, OFPacketIn msg,
            FloodlightContext cntx) {
        
        logger.info("\nNew packet! I gotta do something with it...");
        

        /* Obtain current time. */
        long current_query_time = System.currentTimeMillis();
        float h2_metric = 0;
        float h3_metric = 0;
        
        /* If it's been more than one second since we've last queried the servers, then
         * query them again now.
         */
        if( current_query_time - last_query_time >= 1000) {
        
        	logger.info("Querying servers " + current_query_time + " " + last_query_time);
        	
        	/* Obtain server status. */
	        String status_h2 = ExecuteShellCommand.executeCommand("curl http://" + h2_IP + "/server-status/?auto");
	        String status_h3 = ExecuteShellCommand.executeCommand("curl http://" + h3_IP + "/server-status/?auto");
	        
	        String[] h2_lines = status_h2.split("\n");
	        String[] h3_lines = status_h3.split("\n");
	        
	        int idleWorkers = 0;
	        int busyWorkers = 0;
	        
	        /* Obtain load for h2. */
	        for(int i=0; i<h2_lines.length; i++)
	        	if(h2_lines[i].startsWith("IdleWorkers"))
	        		idleWorkers = Integer.parseInt(h2_lines[i].substring(13));
	        	else if (h2_lines[i].startsWith("BusyWorkers"))
	        		busyWorkers = Integer.parseInt(h2_lines[i].substring(13));
	        
	        h2_metric = ((float )busyWorkers) / (busyWorkers + idleWorkers);
	        logger.info("Busy: " + busyWorkers + " " + "idle: " + idleWorkers);
	        
	        /* Obtain load for h3. */
	        for(int i=0; i<h3_lines.length; i++)
	        	if(h3_lines[i].startsWith("IdleWorkers"))
	        		idleWorkers = Integer.parseInt(h3_lines[i].substring(13));	
	        	else if (h3_lines[i].startsWith("BusyWorkers"))
	        		busyWorkers = Integer.parseInt(h3_lines[i].substring(13));
	        
	        h3_metric = ((float )busyWorkers) / (busyWorkers + idleWorkers);
	
	        logger.info("Busy: " + busyWorkers + " " + "idle: " + idleWorkers);
	        logger.info("Metrics: " + h2_metric + " " + h3_metric);
	        
	        last_query_time = System.currentTimeMillis();
        }
        
    	/* Get the ingress port. */
    	OFPort port = msg.getVersion().compareTo(OFVersion.OF_12) < 0 ? msg.getInPort() : msg.getMatch().get(MatchField.IN_PORT);
    	logger.info("In port: " + port);
        
        /* Get deserialized packet in message. */
        Ethernet eth = IFloodlightProviderService.bcStore.get(
        						cntx,
        						IFloodlightProviderService.CONTEXT_PI_PAYLOAD);
        
        /* Check that packet is of desired type: TCP. */
        if (eth.getEtherType() == EthType.IPv4)
        {
        	logger.info("IPv4 here");
        	IPv4 ipv4 = (IPv4) eth.getPayload();
        	
        	IPv4Address dstIp = ipv4.getDestinationAddress();
        	logger.info("Dest Address: " + dstIp);
        	
        	if(ipv4.getProtocol().equals(IpProtocol.TCP) && port.getPortNumber() == 1) {
        		
				TCP tcp = (TCP) ipv4.getPayload();
				
				short flags = tcp.getFlags();
				
				logger.info("flags:"+flags);
				
				// Used this just to check that the first packet is always a SYN. And it is.
				// No need to add rules based on this check.
				Integer isSYN = flags & 0x0002;
				logger.info(isSYN.toString());
				
				/* Obtain TCP source and destination ports. */
				TransportPort srcPort = tcp.getSourcePort();
				TransportPort dstPort = tcp.getDestinationPort();
				
				logger.info("src port: " + srcPort + "dstPort: "+ dstPort);
				
				/* Decide on which port to send the packets. */
				int outputPort = (h2_metric < h3_metric ? 2 : 3);
				
				/* Set match for packets from h1. */
				Match match_in = (Match)sw.getOFFactory().buildMatch()
					.setExact(MatchField.ETH_TYPE, EthType.IPv4)
					.setExact(MatchField.IP_PROTO, IpProtocol.TCP)
					.setExact(MatchField.TCP_SRC, tcp.getSourcePort())
					.setExact(MatchField.IPV4_SRC, ipv4.getSourceAddress())
					.setExact(MatchField.IN_PORT, OFPort.of(1))
					.build();
				
				/* Set math for packets from h2/h3. */
				Match match_return = (Match)sw.getOFFactory().buildMatch()
					.setExact(MatchField.ETH_TYPE, EthType.IPv4)
					.setExact(MatchField.IP_PROTO, IpProtocol.TCP)
					.setExact(MatchField.IPV4_DST, ipv4.getSourceAddress())
					.build();
				
				/* Set the list of actions for packets from h1. */
				List<OFAction> actions_in = new ArrayList<OFAction>();
				OFAction action_in_1 = sw.getOFFactory().actions().buildOutput()
					.setPort(OFPort.of(outputPort))
					.setMaxLen(0xffFFffFF)
					.build();
				actions_in.add(action_in_1);
				
				/* Set the list of actions for packets from h2/h3. */
				List<OFAction> actions_return = new ArrayList<OFAction>();
				OFAction action_return_1 = sw.getOFFactory().actions().buildOutput()
					.setPort(OFPort.of(1))
					.setMaxLen(0xffFFffFF)
					.build();
				actions_return.add(action_return_1);
				
				/* Add entry for packets from h1. */
				OFFlowMod flow_mod_in = sw.getOFFactory().buildFlowAdd()
					.setMatch(match_in)
					.setIdleTimeout(15)
					.setActions(actions_in)
					.build();
				   
				/* Add entry for packets from h2/h3. */
				OFFlowMod flow_mod_return = sw.getOFFactory().buildFlowAdd()
					.setMatch(match_return)
					.setIdleTimeout(15)
					.setActions(actions_return)
					.build();
				
				/* Apply both entries. */
				sw.write(flow_mod_in);
				sw.write(flow_mod_return);
				
        	} else if(ipv4.getProtocol().equals(IpProtocol.UDP)) {
        		logger.info("UDP here");
        	}
        	
        } else if(eth.getEtherType() == EthType.ARP) {
        	logger.info("\nSetup MAC tables and run again!\n");       	
        }
    }

}

class ExecuteShellCommand {

public static String executeCommand(String command) {

    StringBuffer output = new StringBuffer();

    Process p;
    try {
        p = Runtime.getRuntime().exec(command);
        p.waitFor();
        BufferedReader reader = 
                        new BufferedReader(new InputStreamReader(p.getInputStream()));

        String line = "";           
        while ((line = reader.readLine())!= null) {
            output.append(line + "\n");
        }

    } catch (Exception e) {
        e.printStackTrace();
    }

    return output.toString();

}
}