import java.io.BufferedReader;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

import util.InputParser;
import util.SongProperty;

class Pair1 implements Writable {
    public float loud;
    public int nrSongs;

    public Pair1(float x, int y) {
      this.loud = x;
      this.nrSongs = y;
    }

    public Pair1() {
      this(0, 0);
    }

    public void write(DataOutput out) throws IOException {
      out.writeFloat(loud);
      out.writeInt(nrSongs);
    }

    public void readFields(DataInput in) throws IOException {
      loud = in.readFloat();
      nrSongs = in.readInt();
    }

    public String toString() {
      return Float.toString(loud) + " " + Integer.toString(nrSongs);
    }
}

public class SongCount {

public static class Map extends MapReduceBase 
    implements Mapper<LongWritable, Text, IntWritable, Pair1> {
      Set<SongProperty> mySet;
      
      public Map()
      {
    	  super();
    	  SongProperty[] properties = {SongProperty.LOUDNESS, SongProperty.YEAR};
    	  mySet = new HashSet<SongProperty>(Arrays.asList(properties));
      }
      
      public void map(LongWritable key, Text value, OutputCollector<IntWritable, 
                Pair1> output, Reporter reporter) throws IOException {
        String line = value.toString();
        HashMap<SongProperty, String> song = InputParser.getSongProperties(line, mySet);
        Pair1 p = new Pair1(Float.parseFloat(song.get(SongProperty.LOUDNESS)), 1);
    //    System.out.println(song.toString());
        output.collect(new IntWritable(Integer.parseInt(song.get(SongProperty.YEAR))), p);
      }
    }
  
    public static class Reduce extends MapReduceBase implements 
                             Reducer<IntWritable, Pair1, IntWritable, Pair1> {
      public void reduce(IntWritable key, Iterator<Pair1> values,
                     OutputCollector<IntWritable, Pair1> output, Reporter
                     reporter) throws IOException {
        float sum_loud = 0;
        int sum_count = 0;
        while (values.hasNext()) {
        	Pair1 p = values.next();
        	sum_loud += p.loud * p.nrSongs;
        	sum_count += p.nrSongs;
        }
        sum_loud /= sum_count;
        output.collect(key, new Pair1(sum_loud, sum_count));
      }
}

    public static void main(String[] args) throws Exception {
      JobConf conf = new JobConf(SongCount.class);
      conf.setJobName("SongCount");
  
      conf.setOutputKeyClass(IntWritable.class);
      conf.setOutputValueClass(Pair1.class);
  
      conf.setMapperClass(Map.class);
      conf.setCombinerClass(Reduce.class);
      conf.setReducerClass(Reduce.class);
  
      conf.setInputFormat(TextInputFormat.class);
      conf.setOutputFormat(TextOutputFormat.class);
  
      FileInputFormat.setInputPaths(conf, new Path(args[0]));
      FileOutputFormat.setOutputPath(conf, new Path(args[1]));
 
      JobClient.runJob(conf);
   }
  }