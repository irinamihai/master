import java.io.BufferedReader;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

import util.InputParser;
import util.SongProperty;

class Pair2 implements Writable {
    public float loud_minor;
    public float tempo_minor;
    public int count_minor;
    public float loud_major;
    public float tempo_major;
    public int count_major;

    public Pair2(float x, float y, int z, float x1, float y1, int z1) {
      this.loud_minor = x;
      this.tempo_minor = y;
      this.count_minor = z;
      this.loud_major = x1;
      this.tempo_major = y1;
      this.count_major = z1;
    }

    public Pair2() {
      this(0, 0, 0, 0, 0, 0);
    }

    public void write(DataOutput out) throws IOException {
      out.writeFloat(loud_minor);
      out.writeFloat(tempo_minor);
      out.writeInt(count_minor);
      out.writeFloat(loud_major);
      out.writeFloat(tempo_major);
      out.writeInt(count_major);
    }

    public void readFields(DataInput in) throws IOException {
      loud_minor = in.readFloat();
      tempo_minor = in.readFloat();
      count_minor = in.readInt();
      loud_major = in.readFloat();
      tempo_major = in.readFloat();
      count_major = in.readInt();
    }

    public String toString() {
      return Float.toString(loud_minor) + " " + Float.toString(tempo_minor) + " " + Integer.toString(count_minor) +
    		 " " +  Float.toString(loud_major) + " " + Float.toString(tempo_major) + " " + Integer.toString(count_major);
    }
}

public class MinorMajor {

public static class Map extends MapReduceBase 
    implements Mapper<LongWritable, Text, IntWritable, Pair2> {
      Set<SongProperty> mySet;
      Pair2 p;
      
      public Map()
      {
    	  super();
    	  SongProperty[] properties = {SongProperty.LOUDNESS, SongProperty.TEMPO, SongProperty.MODE, SongProperty.YEAR};
    	  mySet = new HashSet<SongProperty>(Arrays.asList(properties));
      }
      
      public void map(LongWritable key, Text value, OutputCollector<IntWritable, 
                Pair2> output, Reporter reporter) throws IOException {
        String line = value.toString();
        HashMap<SongProperty, String> song = InputParser.getSongProperties(line, mySet);
        
        // if 0, then it's minor
        if (Integer.parseInt(song.get(SongProperty.MODE)) == 0)
            p = new Pair2(Float.parseFloat(song.get(SongProperty.LOUDNESS)),
            		            Float.parseFloat(song.get(SongProperty.TEMPO)), 
            		            1,
            		            0,
            		            0,
            		            0);
        else //if 1, then it's major
        	p = new Pair2(0,
        			      0,
        			      0,
        			      Float.parseFloat(song.get(SongProperty.LOUDNESS)),
        			      Float.parseFloat(song.get(SongProperty.TEMPO)),
        			      1);
        	
        output.collect(new IntWritable(Integer.parseInt(song.get(SongProperty.YEAR))), p);
      }
    }
  
    public static class Reduce extends MapReduceBase implements 
                             Reducer<IntWritable, Pair2, IntWritable, Pair2> {
      public void reduce(IntWritable key, Iterator<Pair2> values,
                     OutputCollector<IntWritable, Pair2> output, Reporter
                     reporter) throws IOException {
        float sum_loud_minor = 0;
        float sum_tempo_minor = 0;
        int sum_count_minor = 0;
        float sum_loud_major = 0;
        float sum_tempo_major = 0;
        int sum_count_major = 0;
        
        while (values.hasNext()) {
        	Pair2 p = values.next();
        	
        	if (p.loud_minor != 0) {
        	    sum_loud_minor += p.loud_minor * p.count_minor;
        	    sum_tempo_minor += p.tempo_minor * p.count_minor;
        	    sum_count_minor += p.count_minor;
        	}
        	else {
        		sum_loud_major += p.loud_major * p.count_major;
        	    sum_tempo_major += p.tempo_major * p.count_major;
        	    sum_count_major += p.count_major;
        	}
        }
        
        if (sum_count_minor != 0) {
            sum_loud_minor /= sum_count_minor;
            sum_tempo_minor /= sum_count_minor;
        }
        
        if (sum_count_major != 0) {
            sum_loud_major /= sum_count_major;
            sum_tempo_major /= sum_count_major;
        }
        
        output.collect(key, new Pair2(sum_loud_minor, sum_tempo_minor, sum_count_minor, sum_loud_major, sum_tempo_major, sum_count_major));
      }
}

    public static void main(String[] args) throws Exception {
      JobConf conf = new JobConf(MinorMajor.class);
      conf.setJobName("MinorMajor");
  
      conf.setOutputKeyClass(IntWritable.class);
      conf.setOutputValueClass(Pair2.class);
  
      conf.setMapperClass(Map.class);
      conf.setCombinerClass(Reduce.class);
      conf.setReducerClass(Reduce.class);
  
      conf.setInputFormat(TextInputFormat.class);
      conf.setOutputFormat(TextOutputFormat.class);
  
      FileInputFormat.setInputPaths(conf, new Path(args[0]));
      FileOutputFormat.setOutputPath(conf, new Path(args[1]));
 
      JobClient.runJob(conf);
   }
  }