import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

import util.Genre;
import util.GenreDetector;
import util.InputParser;
import util.SongProperty;

/* Used for keeping information about the number of song, the average loudness
 * and the average tempo.
 * */
class Pair3 implements Writable {
    public int nrSongs;
    public float loudness;
    public float tempo;

    public Pair3(int x, float y, float z) {
      this.nrSongs = x;
      this.loudness = y;
      this.tempo = z;
    }

    public Pair3() {
      this(0, 0, 0);
    }

    public void write(DataOutput out) throws IOException {
      out.writeInt(nrSongs);
      out.writeFloat(loudness);
      out.writeFloat(tempo);
    }

    public void readFields(DataInput in) throws IOException {
      nrSongs = in.readInt();
      loudness = in.readFloat();
      tempo = in.readFloat();
    }

    public String toString() {
      return Integer.toString(nrSongs) + " " + Float.toString(loudness) + " " + Float.toString(tempo);
    }
}


/* Used for sorting the resulted file. */
class generalPair implements Comparable<generalPair> {
	public float value;
	public int genre;
	
	public generalPair(float v, int genre) {
		this.value = v;
		this.genre = genre;
	}
	
	@Override
	public int compareTo(generalPair gp) {
		
		if (this.value < gp.value)
			return -1;
		
		if (this.value > gp.value)
			return 1;
		
		return 0;
	}
}

public class Sorting {

public static class Map extends MapReduceBase 
    implements Mapper<LongWritable, Text, IntWritable, Pair3> {
      Set<SongProperty> mySet;
      
      public Map()
      {
    	  super();
    	  SongProperty[] properties = {SongProperty.LOUDNESS, SongProperty.TEMPO, SongProperty.ARTIST_TERMS, SongProperty.ARTIST_TERMS_FREQ};
    	  mySet = new HashSet<SongProperty>(Arrays.asList(properties));
      }
      
      public void map(LongWritable key, Text value, OutputCollector<IntWritable, 
                Pair3> output, Reporter reporter) throws IOException {
        String line = value.toString();
        HashMap<SongProperty, String> song = InputParser.getSongProperties(line, mySet);
        
        
        /* As suggested in GenreDetector.java, we need the ARTIST_TERMS and ARTIST_TERMS_FREQ
         * to determine the genre of a song.
         */
        
        // Obtain the terms.
        String[] terms = InputParser.stringToArrayString(song.get(SongProperty.ARTIST_TERMS));
        
        /* Obtain the importances in a string and transform them into floats, as we need them
         * in the detectGenre function.
         */
        String[] importancesString = InputParser.stringToArrayString(song.get(SongProperty.ARTIST_TERMS_FREQ));
        float[] importancesFloat = new float[importancesString.length];
        
        for(int i=0; i<importancesString.length; i++)
        	if (importancesString[i] != null && !importancesString[i].isEmpty())
        	    importancesFloat[i] = Float.parseFloat(importancesString[i]);
        	else
        		importancesFloat[i] = 0;
       
        // Obtain the genre.
        Genre genre = (new GenreDetector()).detectGenre(terms, importancesFloat);
        
        Pair3 p = new Pair3(1, Float.parseFloat(song.get(SongProperty.LOUDNESS)), Float.parseFloat(song.get(SongProperty.TEMPO)));
        
        output.collect(new IntWritable(genre.ordinal()), p);
      }
    }
  
    public static class Reduce extends MapReduceBase implements 
                             Reducer<IntWritable, Pair3, IntWritable, Pair3> {
      public void reduce(IntWritable key, Iterator<Pair3> values,
                     OutputCollector<IntWritable, Pair3> output, Reporter
                     reporter) throws IOException {
        int nrSongs = 0;
    	float sum_loud = 0;
        float sum_tempo = 0;
        while (values.hasNext()) {
        	Pair3 p = values.next();
        	nrSongs += p.nrSongs;
        	sum_loud += p.loudness * p.nrSongs;
        	sum_tempo += p.tempo * p.nrSongs;
        }
        
        if (nrSongs != 0) {
            sum_loud /= nrSongs;
            sum_tempo /= nrSongs;
        }
        output.collect(key, new Pair3(nrSongs, sum_loud, sum_tempo));
      }
}

    public static void main(String[] args) throws Exception {
      JobConf conf = new JobConf(Sorting.class);
      conf.setJobName("Sorting");
  
      conf.setOutputKeyClass(IntWritable.class);
      conf.setOutputValueClass(Pair3.class);
  
      conf.setMapperClass(Map.class);
      conf.setCombinerClass(Reduce.class);
      conf.setReducerClass(Reduce.class);
  
      conf.setInputFormat(TextInputFormat.class);
      conf.setOutputFormat(TextOutputFormat.class);
  
      FileInputFormat.setInputPaths(conf, new Path(args[0]));
      FileOutputFormat.setOutputPath(conf, new Path(args[1]));
 
      JobClient.runJob(conf);
      
      /* ArrayLists where the values from the obtained file will be kept. */
      ArrayList <generalPair> nrSongSorted = new ArrayList<generalPair> ();
      ArrayList <generalPair> loudnessSorted = new ArrayList<generalPair> ();
      ArrayList <generalPair> tempoSorted = new ArrayList<generalPair> ();
      
      File sortedGenresFile = new File(args[1] + "/part-00000");
      
      /* Read each line and assign the values to the appropriate ArrayList. */
      try(BufferedReader br = new BufferedReader(new FileReader(sortedGenresFile))) {
    	    for(String line; (line = br.readLine()) != null; ) {
    	        String[] values = line.split("\t| ");
    	        nrSongSorted.add(new generalPair(Float.parseFloat(values[1]), Integer.parseInt(values[0])));
    	        loudnessSorted.add(new generalPair(Float.parseFloat(values[2]), Integer.parseInt(values[0])));
    	        tempoSorted.add(new generalPair(Float.parseFloat(values[3]), Integer.parseInt(values[0])));
    	    }
    	    
    	    br.close();
    	}
      
      /* Sort the ArrayLists. */
      Collections.sort(nrSongSorted);
      Collections.sort(loudnessSorted);
      Collections.sort(tempoSorted);
      
      
      /* Write the results to a separate file. */
      BufferedWriter outWriter = null;
      
      try {
    	  FileWriter fstream  = new FileWriter(args[1] +"/sortedFile", true);
    	  outWriter = new BufferedWriter(fstream);
    	  
    	  outWriter.write("\n    Number of songs\n");
          for (int i=0; i<nrSongSorted.size(); i++)
        	  outWriter.write(Genre.values()[nrSongSorted.get(i).genre].toString() + " " + ((int)nrSongSorted.get(i).value + "\n"));
      
          outWriter.write("\n    Loudness\n");
          for (int i=0; i<loudnessSorted.size(); i++)
        	  outWriter.write(Genre.values()[loudnessSorted.get(i).genre].toString() + " " + loudnessSorted.get(i).value + "\n");
      
          outWriter.write("\n    Tempo\n");
          for (int i=0; i<tempoSorted.size(); i++)
    	      outWriter.write(Genre.values()[tempoSorted.get(i).genre].toString() + " " + tempoSorted.get(i).value + "\n");
      }
      catch (IOException e) {
    	  System.err.println("Error: " + e.getMessage());
      }
      finally {
    	  if (outWriter != null)
    		  outWriter.close();
      } 
   }
}

