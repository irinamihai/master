import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

import util.Genre;
import util.GenreDetector;
import util.InputParser;
import util.SongProperty;

class Pair4 implements Writable {
    public float[] values = new float[4];
    public int count = 0;

    public Pair4() {
    	 for(int i=0; i<4; i++)
    	 {
    		 values[i] = 0;
    	 }
    }

    public void write(DataOutput out) throws IOException {
    	for(int i=0; i<4; i++)
    	{
    		out.writeFloat(values[i]);
    	}
		out.writeInt(count);
    }

    public void readFields(DataInput in) throws IOException {
    	for(int i=0; i<4; i++)
    	{
    		values[i] = in.readFloat();
    	}
		count = in.readInt();
    }

    public String toString() {
      String s = new String();
      for(int i=0; i<4; i++)
    	  s += values[i] + " ";
      return s;
    }
}

public class Crossover {

public static class Map extends MapReduceBase 
    implements Mapper<LongWritable, Text, IntWritable, Pair4> {
      Set<SongProperty> mySet;
	  public static SongProperty[] properties = {SongProperty.DURATION, SongProperty.LOUDNESS, SongProperty.MODE, SongProperty.TEMPO,
			  SongProperty.ARTIST_TERMS, SongProperty.ARTIST_TERMS_FREQ};
	  
      public Map()
      {
    	  super();
    	  mySet = new HashSet<SongProperty>(Arrays.asList(properties));
      }
      
      public void map(LongWritable key, Text value, OutputCollector<IntWritable, 
                Pair4> output, Reporter reporter) throws IOException {
        String line = value.toString();
        HashMap<SongProperty, String> song = InputParser.getSongProperties(line, mySet);
        
        /* As suggested in GenreDetector.java, we need the ARTIST_TERMS and ARTIST_TERMS_FREQ
         * to determine the genre of a song.
         */
        
        // Obtain the terms.
        String[] terms = InputParser.stringToArrayString(song.get(SongProperty.ARTIST_TERMS));
        
        /* Obtain the importances in a string and transform them into floats, as we need them
         * in the detectGenre function.
         */
        String[] importancesString = InputParser.stringToArrayString(song.get(SongProperty.ARTIST_TERMS_FREQ));
        float[] importancesFloat = new float[importancesString.length];
        
        for(int i=0; i<importancesString.length; i++)
        	if (importancesString[i] != null && !importancesString[i].isEmpty())
        	    importancesFloat[i] = Float.parseFloat(importancesString[i]);
        	else
        		importancesFloat[i] = 0;
       
        // Obtain the genre.
        Genre genre = (new GenreDetector()).detectGenre(terms, importancesFloat);
        
        Pair4 p = new Pair4();
        p.count = 1;
        for(int i=0; i<4; i++)
        {
        	p.values[i] = Float.parseFloat(song.get(properties[i]));
        }
 
        output.collect(new IntWritable(genre.ordinal()), p);
      }
    }
  
    public static class Reduce extends MapReduceBase implements 
                             Reducer<IntWritable, Pair4, IntWritable, Pair4> {
      public void reduce(IntWritable key, Iterator<Pair4> values,
                     OutputCollector<IntWritable, Pair4> output, Reporter
                     reporter) throws IOException {
        Pair4 out = new Pair4();
        while (values.hasNext()) {
        	Pair4 p = values.next();
        	for(int i=0; i<4; i++)
        		out.values[i] += p.values[i] * p.count;
        	out.count += p.count;
        }
        for(int i=0; i<4; i++)
        	out.values[i] /= out.count;
        output.collect(key, out);
      }
}

    public static void main(String[] args) throws Exception {
      JobConf conf = new JobConf(Crossover.class);
      conf.setJobName("Crossover");
  
      conf.setOutputKeyClass(IntWritable.class);
      conf.setOutputValueClass(Pair4.class);
  
      conf.setMapperClass(Map.class);
      conf.setCombinerClass(Reduce.class);
      conf.setReducerClass(Reduce.class);
  
      conf.setInputFormat(TextInputFormat.class);
      conf.setOutputFormat(TextOutputFormat.class);
  
      FileInputFormat.setInputPaths(conf, new Path(args[0]));
      FileOutputFormat.setOutputPath(conf, new Path(args[1]));
 
      JobClient.runJob(conf);
      
      @SuppressWarnings("unchecked")
	  ArrayList<Float>[] values = (ArrayList<Float>[])new ArrayList[4];
      for(int i=0; i<4; i++)
    	  values[i] = new ArrayList<Float>();
      
	  File file = new File(args[1] + "/part-00000");
      try(BufferedReader br = new BufferedReader(new FileReader(file))) {
    	    for(String line; (line = br.readLine()) != null; ) {
    	        String[] tokens = line.split("\t| ");
    	        values[0].add(Float.parseFloat(tokens[1]));
    	        values[1].add(Float.parseFloat(tokens[2]));
    	        values[2].add(Float.parseFloat(tokens[3]));
    	        values[3].add(Float.parseFloat(tokens[4]));
    	    }
    	    br.close();
    	}
      
      // Transforming elements into [0,1] range.
      for(int i=0; i<4; i++) {
    	  float min = (float)1E20;
    	  float max = (float)-1E20;
    	  for(int j=0; j<values[i].size(); j++) {
    		  if(values[i].get(j) < min)
    			  min = values[i].get(j);
    		  if(values[i].get(j) > max)
    			  max = values[i].get(j);
    	  }
    	  
    	  if(max != min) {
	    	  for(int j=0; j<values[i].size(); j++) {
	    		  values[i].set(j, (values[i].get(j)-min)/(max-min));
	    	  }
    	  }
      }
      
      // Computing the final matrix.
      float[][] M = new float[values[0].size()][values[0].size()];
      for(int i=0; i<values[0].size(); i++)
    	  for(int j=i; j<values[0].size(); j++) {
    		  M[i][j] = 0;
    		  for(int k=0; k<4; k++) {
    			  M[i][j] += Math.abs(values[k].get(i) - values[k].get(j));
    		  }
    		  M[i][j] = (float)Math.exp(-M[i][j]);
    		  M[j][i] = M[i][j];
    	  }

      // Printing
      BufferedWriter outWriter = null;
      
      try {
    	  FileWriter fstream  = new FileWriter(args[1] +"/crossoverFile", true);
    	  outWriter = new BufferedWriter(fstream);
          outWriter.write("\n            \t");
          for(int i=0; i<values[0].size(); i++)
        	  outWriter.write(" "  + (Genre.values()[i].toString() + "    ").substring(0, 4));
      
          outWriter.write("\n");
      
          for(int i=0; i<values[0].size(); i++) {
        	  outWriter.write(Genre.values()[i].toString() + "      \t");
			  for(int j=0; j<values[0].size(); j++) {
				  outWriter.write(String.format(" %1.2f", M[i][j]));
			  }
              outWriter.write("\n");
          }
      }
      catch (IOException e) {
    	  System.err.println("Error: " + e.getMessage());
      }
      finally {
    	  if (outWriter != null)
    		  outWriter.close();
      } 
   }
 }